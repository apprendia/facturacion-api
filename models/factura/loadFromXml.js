module.exports = function () {
  return function(xmlString){
    const parseString = require('xml2js').parseString
    const Promise = require('promise')

    return new Promise(function(resolve, reject){
      const opts = {
        // async: true
      }

      parseString(xmlString, opts, function (err, result) {
        if( err ) return reject( new Error(err) )
        resolve( buildFacturaObj( result ) )
      })
    })
  }

  function buildFacturaObj( parsed ){
    const _ = require('lodash')
    let factura = {}
    const cfdi = _.get(parsed, 'cfdi:Comprobante')


    // $ - Factura attributes
    factura.total = Number(_.get(cfdi, '$.Total', 0))
    factura.serie = _.get(cfdi, '$.Serie')
    factura.folio = Number(_.get(cfdi, '$.Folio', 0))
    factura.fecha = _.get(cfdi, '$.Fecha')
    

    // cfdi:Complemento.0.tfd:TimbreFiscalDigital.0 - Timbre attributes
    const timbre = _.get(cfdi, 'cfdi:Complemento.0.tfd:TimbreFiscalDigital.0')
    factura.uuid = _.get(timbre, '$.UUID')

    return factura
  }
}()