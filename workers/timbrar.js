'use strict';

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

module.exports = function () {
  let Promise = require('promise')
  let path = require('path')
  // let rootPath = path.resolve('')
  let util = require('util')
  let chdir = require('in-folder')

  // //console.log('rootPath', rootPath)

  return {
    run: run,
    initCfdi: initCfdi,
    signCfdi: signCfdi,
    notifyWebhook: notifyWebhook,
    onTimbrarRequestAdded: onTimbrarRequestAdded
  }

  function run() {
    // Starts listening when TimbrarRequests are added
    require("../config/admin").db.ref('timbrarRequests').orderByChild('status').equalTo(null).on('child_added', onTimbrarRequestAdded)
  }

  function getFirstObj(obj) {
    return obj[Object.keys(obj)[0]];
  }

  var account_id;
  var recarga_id;

  // On TimbrarRequest added
  async function onTimbrarRequestAdded(timbrarRequestSnap) {
	  let timbrarRequest
    try {
	         timbrarRequest = timbrarRequestSnap.val()


    if (!timbrarRequest || timbrarRequest.status == 2 || timbrarRequest.error || !timbrarRequest.factura || timbrarRequest.viaRest === true) {
	    console.error('cant start onTimbrarRequestAdded')
	    if(!timbrarRequest){
				console.error('no timbrarRequest')  
	    }else{
				console.error('timbrarRequest.status ', timbrarRequest.status, ' timbrarRequest.error ', timbrarRequest.error, ' timbrarRequest.factura ', timbrarRequest.factura, ' timbrarRequest.viaRest ', timbrarRequest.viaRest)  		    
	    }
      return false
    }
    
    
		if(!timbrarRequest.account){
			let root = timbrarRequestSnap.ref.root
			await (root.child('facturas').child(timbrarRequest.factura).once('value').then((snap)=>{
				if(snap.val() && snap.val().path){
					return root.child(snap.val().path).once('value').then((facturaSnap)=>{
						return (timbrarRequest.account = facturaSnap.ref.parent.parent.key)
					})
				}
			}))
    }

    account_id = timbrarRequest.account;
    
    if(!account_id){
	    throw 'Account id for this factura was not found'
    }



    // require("../config/admin").db.ref(`/accounts/${timbrarRequest.account}/recargas`).orderByChild('activo').equalTo(true).limitToLast(1).once('value').then((snap)=>{
    //   let recarga = getFirstObj(snap.val())
    //   //console.log(recarga)
    // })
    
//      checkForCredits(timbrarRequestSnap)
//        .then(getFactura)
			getFactura(timbrarRequestSnap)
        .then(loadFactura)
        .then(initCfdi)
        .then(signCfdi)
        .then(sendToTimbrar)
        .then(updateCredits)
        .then(function ([factura, result]) {

          let { account, factura:facturaId } = timbrarRequestSnap.val();



          // if(!factura.prueba)
          //    require("../config/admin").db.ref('')
          let promises = []
          console.log("factura timbrada: ", account, facturaId);

          let recarga = (factura.prueba) ? null : recarga_id;

          // Update Factura
          if (result && (result.uuid || result.fechaTimbrado)) {
            // Update Timbrar Request
            promises.push(timbrarRequestSnap.ref.update({ status: 2, error: null }))

            let attrs = {
              uuid: result.uuid,
              xuuid: result.uuid,
              fechaTimbrado: result.fechaTimbrado,
              recarga: recarga || null
            }
            // if(!factura.prueba){
            //   require("../config/admin").db.ref('accounts').child(account).once('value').then((snap)=>{
            //     let creditos = snap.val().creditos -1
            //     return snap.ref.update({creditos: creditos})
            //   })

            // }

            promises.push(factura.ref.update(attrs))
          }

          return Promise.all(promises).then(() => factura.ref.once('value')).then((snap) => [snap, result])
        })
        .then(generateAttachments)
//        .then(notifyWebhook)
        .catch(function (e) {
          //console.log("aqui 1")
          console.log('timbrarRequestSnap: ', timbrarRequestSnap.key, ' factura: ', timbrarRequest.factura, ' error:', e)
          timbrarRequestSnap.ref.update({ status: 3 })
          if(e){
	          timbrarRequestSnap.ref.update({ error: `${e.message || util.inspect(e, false, null)}` })
          }
        })
    } catch (e) {
	    if(!timbrarRequestSnap || !timbrarRequest){
		          console.log(' error:', e)
		          return false
	    }
      //console.log("aqui")

      console.log('timbrarRequestSnap: ', timbrarRequestSnap.key, ' factura: ', timbrarRequest.factura, ' error:', e)
      timbrarRequestSnap.ref.update({ status: 3 })
      timbrarRequestSnap.ref.update({ error: `${util.inspect(e, false, null)}` })
    }
  }

//   function checkForCredits(timbrarRequestSnap) {
// 	  return new Promise(function(resolve, reject) {
// 	//	  console.log('checkForCredits')
// //		  return resolve(timbrarRequestSnap);
		  
// 	    return require("../config/admin").db.ref(`/accounts/${account_id}/recargas`).orderByChild('activo').equalTo(true).limitToLast(1).once('child_added', (snap) => {
// 	      let recargaRef = snap.ref
// 	      recarga_id = snap.key
	      
	      
// // 	      console.log('snap val ', snap.val(), ' recargaRef ', `${recargaRef}`)
	
// 	      if (!recargaRef){
// 		      return reject({ phase: 'checkForCredits', error: 'No items recarga found' }) 
// 	      }
	
// 	      var canContinue = false;
// 	      return recargaRef.transaction(function(recarga) {
// 	        if (recarga) {
	
// 	          let currentFacturas = (recarga.facturas) ? Object.keys(recarga.facturas).length : 0
	          
// // 	          console.log('recarga', recarga, 'currentFacturas count ', currentFacturas, ' facturasCount ', recarga.facturasCount, 'recarga.creditos', recarga.creditos)
	          
// 	          canContinue = (currentFacturas < recarga.creditos)
// 	        }
// 	        return recarga
// 	      }).then(() => {
// 	        return (canContinue) ? resolve(timbrarRequestSnap) : reject({ phase: 'checkForCredits', error: 'No credits left' })
// 	      })
// 	    }).catch(reject)
// 	  })
// 	}

  function getFactura(timbrarRequestSnap) {
    return new Promise(function (resolve, reject) {
      let timbrarRequest = timbrarRequestSnap.val()

      if (!timbrarRequest || !timbrarRequest.factura) {
        return reject('Missing factura id')
      }
      let facturaId = timbrarRequest.factura
      
      
      let onFactura = function (snap) {
	          if (!snap.exists()) {
	            return reject({ phase: 'getFactura', error: `Factura ${snap.ref.path} doesnt exist` });
	          }
	          return resolve(snap);
	        }
      
      if(!timbrarRequest.account){
	      timbrarRequestSnap.ref.root.child('facturas').child(facturaId).once('value').then((snap)=>{
		      if(snap.val() && snap.val().path){
			      return snap.ref.root.child(snap.val().path).once('value').then(onFactura).catch(reject)
		      }
	      }).catch(reject) 
      }else{
	      getAccountRef(timbrarRequestSnap).then(function (accountRef) {
	        return accountRef.child('facturas').child(facturaId).once('value').then(onFactura)
	      }).catch(reject) 
      }
    })
  }

  function getAccountRef(timbrarRequestSnap) {
    return new Promise(function (resolve, reject) {
      let timbrarRequest = timbrarRequestSnap.val();

      if (!timbrarRequest || !timbrarRequest.account) {
        return reject({ phase: 'getAccountRef', error: 'No account or timbrarRequest' })
      }

      let accountId = timbrarRequest.account

      timbrarRequestSnap.ref.root.child('accounts').child(accountId).once('value').then(function (snap) {
        if (!snap.exists()) {
          return reject({ phase: 'getAccountRef', error: 'No account or timbrarRequest' });
        }
        return resolve(snap.ref);
      }).catch(reject);
    })
  }

  function loadFactura(facturaSnap) {
    //console.log("loading factura")
    return new Promise(function (resolve, reject) {
      try {
        let Factura = require('../models/factura')
        return Factura.load(facturaSnap).then(resolve)
      } catch (e) {
        reject({
          phase: 'loadFactura',
          error: e
        })
      }
    })
  }

  function initCfdi(factura) {
    //console.log("init cdfi")
    return new Promise(function (resolve, reject) {
      try {
        let FacturaXML = require('../serializers/factura-xml')
        factura.xmlDoc = FacturaXML(factura)
        resolve(factura)
      } catch (e) {
        reject({
          phase: 'initCfdi',
          error: e
        })
      }
    })
  }

  function signCfdi(factura) {
    //console.log("sign")
    return new Promise(function (resolve, reject) {
      try {
        let xml = factura.xmlDoc
        if (!xml) { throw 'No xmlDoc for Factura' }


        let certificado = factura.certificado.pemCer.toString().trim().split(/\r?\n/)
        certificado.pop()
        certificado.shift()
        let cert = certificado.join('')


        xml.root().att({
          NoCertificado: factura.certificado.numero,
          Certificado: cert
        })

        generateCadenaOriginal(xml).then(function (cadena) {
          let sello = sign64(cadena, factura.certificado.pemKey)
          xml.root().att({ Sello: sello })
          factura.cadenaOriginal = cadena
        }).then(function () {

          return validateXSD(xml).then(function () {
            // process.chdir(path.resolve(rootPath))
            resolve(factura)
          }).catch(reject)

        }).catch(function (e) {
          throw e
        })
      } catch (e) {
        reject({
          phase: 'signCfdi',
          error: e
        })
      }
    })
  }

  function generateCadenaOriginal(doc) {
    return new Promise(function (resolve, reject) {
      try {
        let libxslt = require('libxslt')

        chdir(global.rootPath('config/zfactu/sat'), function () {
          return new Promise(function (resolve, reject) {
            libxslt.parseFile('cadenaoriginal_3_3.xslt', function (err, stylesheet) {
              if (err) { reject(err) } else {
                let docStr = doc.end(); // it should be string
                stylesheet.apply(docStr, function (err, result) {
                  // process.chdir(path.resolve(rootPath))
                  if (err) { reject(err) } else { resolve(result) }
                })
              }
            })
          }).then(resolve).catch(reject)
        })
      } catch (e) { reject(e) }
    })
  }

  function sign64(cadena, keyPem) {
    let forge = require('node-forge')
    let pki = forge.pki
    let privateKey = pki.privateKeyFromPem(keyPem)
    let digest = forge.md.sha256.create()

    digest.update(cadena, 'utf8')

    return forge.util.encode64(privateKey.sign(digest)).replace("\n", '');
  }

  function validateXSD(doc) {
    // process.chdir(path.resolve(__dirname))



    //console.log("validations xsd")
//       console.log(doc.end())
    return new Promise(async function (resolve, reject) {
      let xmlDoc = require('libxslt').libxmljs.parseXml(doc.end())
      let xsdDoc = await require(global.rootPath('services/cfdv33xsd'))

      debugger

      chdir(global.rootPath('config/zfactu/sat'), function () {
        return new Promise(function (resolve, reject) {
          debugger

          xsdDoc.validate(xmlDoc, function (err, validationErrors) {
            debugger

            if (err || validationErrors) {
              return reject(err || validationErrors)
            }
            resolve(true)
          })
        }).then(resolve).catch(reject)
      })
    })
  }

  function sendToTimbrar(factura) {
    //process.chdir(path.resolve(rootPath))
    //console.log("llego a intento timbrar")

    return new Promise(function (resolve, reject) {
      if (!factura || !factura.certificado) {
        return reject()
      }

      if (factura.uuid) {
        return resolve([factura])
      }

      let pac = require(global.rootPath('services/pacs/edicom'))
      let pacOpts = {
        prueba: factura.testOnly || factura.prueba,
        id: factura.id
      }
//       console.log(factura.xmlDoc.end())
      return pac.timbrar(factura.xmlDoc.end(), pacOpts).then(function (result) {
        return resolve([factura, result])
      }).catch(reject)
    })
  }

  function updateCredits([factura, result]) {
    return new Promise(function (resolve, reject) {
      // let db = require("../config/admin").db

      // if (factura.prueba)
      //   return resolve([factura, result])


      // return db.ref(`/accounts/${account_id}/recargas/${recarga_id}`).transaction(function (recarga) {
      //   if (recarga) {
      //     if (!recarga.facturas)
      //       recarga.facturas = {}
      //     recarga.facturas[factura.id] = true

      //     let currentFacturas = (recarga.facturas) ? Object.keys(recarga.facturas).length : 0
      //     if (currentFacturas == recarga.creditos)
      //       recarga.activo = false


      //   }

      //   return recarga
      // }).then(() => {
        return resolve([factura, result])
      // })



    })

  }

  // Get certificado
  function getCertificadoFromFactura(facturaSnap) {
    return new Promise(function (resolve, reject) {
      let certificadoId = facturaSnap.val().certificado;
      if (!certificadoId) {
        return reject()
      }

      let accountRef = facturaSnap.ref.parent.parent;

      accountRef.child('fiscalCertificados').child(certificadoId).once('value').then(function (snap) {
        if (!snap.exists()) {
          return reject();
        }
        return resolve(snap.val());
      }).catch(reject);
    });
  }

  function generateAttachments([facturaSnap, result]) {
    //process.chdir(path.resolve(rootPath))

    let exportable = require(global.rootPath('exportable'))

    debugger

    if (!result) { return Promise.reject({ phase: 'generateAttachments', error: 'No result XML' }) }

    return exportable.buildXML(facturaSnap, `${result.xml}`).then(function (url) {
      return facturaSnap.ref.update({ xmlUrl: url }).then(function () {
        return facturaSnap.ref.once('value');
      })
    }).then(function (facturaSnap) {
      return facturaSnap.ref.update({ pdfUrl: `https://api.miscfdis.com/facturas/${facturaSnap.key}.pdf` }).then(function() {
        return facturaSnap.ref.once('value');
      })
      // return exportable.buildPDF(facturaSnap).then(function (url) {
      //   return facturaSnap.ref.update({ pdfUrl: url }).then(function () {
      //     return facturaSnap.ref.once('value');
      //   })
      // })
    })
  }


  function notifyWebhook(facturaSnap) {
    // process.chdir(path.resolve(rootPath))
    let factura = facturaSnap.val();
    let webhookUrl = factura.webhookUrl || factura.successWebhookUrl; // || 'http://localhost:4000/dummy/webhook';

    if (webhookUrl) {
      let webhookWorker = new require(global.rootPath('workers/webhooks'))
      let facturaJson = require(global.rootPath('serializers/factura-json'))

      return new facturaJson(facturaSnap).serialize().then(function (json) {
        return webhookWorker.success(webhookUrl, json).catch(function(response){
	        facturaSnap.ref.update({
            webhookError: `${response.error}`
          });

          webhookWorker = null;
          facturaJson = null;
        }).then(function () {
          facturaSnap.ref.update({
            webhookSent: true
          });

          webhookWorker = null;
          facturaJson = null;
        });
      });
    }
  }



}();
