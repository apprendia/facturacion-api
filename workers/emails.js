module.exports = function () {
	let db = require(global.rootPath("config/admin")).db;
	db.ref('emailRequests').orderByChild('status').equalTo(null).on('child_added', onEmailRequestAdded);

	return {
		sendFacturaEmail: sendFacturaEmail
	}


	function onEmailRequestAdded(emailRequestSnap) {
		let db = require("../config/admin").db;

		let emailRequest = emailRequestSnap.val(),
			facturaId = emailRequest.factura,
			accountId = emailRequest.account;

		if (!facturaId || !accountId || emailRequest.status == 2) {
			return
		};

		let accountRef = db.ref('accounts').child(accountId);
		let facturaRef = accountRef.child('facturas').child(facturaId);

		facturaRef.once('value').then(function (snap) {
			debugger
			return sendFacturaEmail(snap, emailRequest.to);
		}).then(function () {
			accountRef = null;
			facturaRef = null;
			db = null;
			emailRequestSnap.ref.update({
				status: 2
			});
			emailRequestSnap = null;
		}).catch(function(){
			emailRequestSnap.ref.update({
				status: 3
			});
		});
	}



	async function sendFacturaEmail(snap, to) {
		let Promise = require('promise')
		let mail = require('@sendgrid/mail')


		let factura = await require(global.rootPath('./models/factura')).load(snap)


		return new Promise(async function (resolve, reject) {
			try {
				mail.setApiKey('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')
				
				const msg = {
					to: to,
					from: `${factura.emisor.nombreComercial || factura.emisor.nombre} <deliver@facturacion.contamc.com>`,
					replyTo: `${factura.emisor.nombreComercial || factura.emisor.nombre} <${factura.emisor.email}>`,
					subject: `Entrega de factura electrónica ${factura.uuid}`,
					html: '<p></p>',
					templateId: factura.emailtemplateId || 'b2bb67bd-7166-4cdb-a302-c5db6690165e',
					substitutions: {
						receptorName: `${factura.receptor.nombre}`,
						uuid: `${factura.uuid}`,
						emisorEmail: `${factura.emisor.email}`,
						emisorName: `${factura.emisor.nombreComercial || factura.emisor.nombre}`,
						re: `${factura.emisor.rfc}`,
						rr: `${factura.receptor.rfc}`,
						tt: factura.total.toFixed(2),
						fe: `${factura.selloCFD}`.slice(-8),
						pdfUrl: `https://miscfdis-api.herokuapp.com/facturas/${factura.uuid}.pdf`,
						xmlUrl: factura.xmlUrl
					},
					attachments: [{
							content: await getBase64Attachment(factura.xmlUrl),
							filename: `${factura.uuid}.xml`,
						},
						{
							content: await getBase64Attachment(`https://miscfdis-api.herokuapp.com/facturas/${factura.uuid}.pdf`), //factura.pdfUrl
							filename: `${factura.uuid}.pdf`,
						}
					],
				};
				resolve(mail.send(msg));
			} catch (e) {
				reject(e)
			}
		})
	}


	function getBase64Attachment(url) {
		let tmp = require('tmp')

		function base64Encode(file) {
			debugger
			let fs = require('fs')
			let bitmap = fs.readFileSync(file)
			return new Buffer(bitmap).toString('base64')
		}

		return new Promise(function (resolve) {
			tmp.file(function _tempFileCreated(err, path, fd, cleanupCallback) {
				try{
					if (err) throw err

					let Promise = require('promise')
					let https = require('https')
					let fs = require('fs')

					let file = fs.createWriteStream(path)
					debugger
					https.get(url, function (response) {
						debugger
						response.pipe(file)

						debugger
						file.on("finish", () => { resolve(base64Encode(file.path)) })
					})
				} catch(e){
					resolve(null)
				}
			})
		})
	}


}();
