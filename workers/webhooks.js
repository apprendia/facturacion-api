'use strict';

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

module.exports = function () {
  const Promise = require('promise')
  const path = require('path')
  const util = require('util')
	const _ = require('underscore');
	const request = require('request');

  // //console.log('rootPath', rootPath)

  return {
    run: run
  }

  function run() {
    // Starts listening when Webhooks are added
    require("../config/admin").db.ref('/webhooks').on('child_added', onWebhookAdded)
  }
  
  function onWebhookAdded(webhookSnap){
	  console.log('> webhookSnap', webhookSnap.key)
	  
    const webhook = webhookSnap.val(), payload = webhook.payload || {}, tries = webhook.tries
    
		if (!tries || isNaN(tries)) return false
		if (tries > 10) return false

    try {
      if (!webhook.type) {
        throw 'Missing webhook.type'
      }

      if (!webhook.method || !webhook.url) {
        throw 'Missing webhook.method or webhook.url'
      }

      return request[webhook.method](webhook.url, {
        json: payload || {}
      }, function(error, response, body) {
        if (error || (response && response.statusCode >= 400)) {
          console.error(`Error Webhook Response ${webhookSnap.key}:`, error)
          webhookSnap.ref.update({
            error: `${error} `,
            tries: (tries + 1)
          })
        } else {
          // Delete this webhook
          return webhookSnap.ref.remove()
        }
      })
    } catch (error) {
      console.error(`Error Webhook Execution ${webhookSnap.key}:`, error)

      return webhookSnap.ref.update({ error: `${error} `})
    }
  }
}()/*


module.exports = function(){
	// Utils
	let _ = require('underscore');
	let Promise = require('promise');
	const request = require('request');
	
	function successRequest(url, json) {
		return new Promise(function(resolve, reject) {
			request.post(
				{
					url: url,
					json: json,
					rejectUnauthorized: false,
					strictSSL: false,
					agentOptions: {
						securityOptions: 'SSL_OP_NO_SSLv3'
					}
				},
				function(error, response, body) {
					if (error) {
						reject({
							error: error,
							response: response,
							body: body
						});
					} else {
						resolve();
					}
				}
			);
		});
	}
	
	return {
		success: successRequest
	};
}();
*/





