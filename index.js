'use strict';

console.log('process.env.env', process.env.env)
require('newrelic')

let path = require('path')
global._rootPath = path.resolve('')
global.rootPath = function(){ return require('path').resolve(global._rootPath, ...arguments) }

require('./config/initializers/string')
require('./config/initializers/number-to-words')

require('./services/cfdv33xsd').then(()=>{
	process.chdir(global.rootPath())

//	debugger

	require('./config/router')()
	//require('./workers/timbrar').run()
	require('./workers/cancelar')
	require('./workers/emails')
	require('./workers/webhooks').run()

//	setTimeout(function(){ process.exit(1) }, 60 * 60 * 1000)
})