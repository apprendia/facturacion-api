module.exports = function () {
  // Utils
  //const _ = require('underscore');
  let Promise = require('promise')
  let moment = require('moment')


  let config = {
    action: 'read',
    expires: moment().add(5, 'years').format()
  }


  return {
    buildXML: buildXML,
    buildPDF: buildPDF,
    updatePDF: buildAndSavePdf
  }



  function buildXML(facturaSnap, xml, cancelado) {
    return new Promise(function (resolve, reject) {
      try {
        // debugger

        let admin = require("./config/admin").app
        let bucket = admin.storage().bucket()

        let fileName = generateFilename(facturaSnap, cancelado)
        debugger
        let file = bucket.file(`${fileName}.xml`.replace('-',''))

        let uploadStream = file.createWriteStream({ resumable: false }).on('error', (err) => {
          reject(err)
        }).on('finish', function ( /*file, path*/ ) {
          return file.getSignedUrl(config).then((data) => {
            let url = data[0]
            resolve(url)
          })
        })

        uploadStream.write(xml)
        uploadStream.end()
      } catch (e) {
        reject(e)
      }
    })
  }






  function buildPDF(facturaSnap) {
    return new Promise(function (resolve, reject) {
      try {
        let Factura = require('./models/factura')
        return Factura.load(facturaSnap).then(async function (factura) {
          debugger
          let admin = require("./config/admin").app
          let bucket = admin.storage().bucket()

          let fileName = generateFilename(facturaSnap)
          let file = bucket.file(`${fileName}.pdf`.replace('-',''))

          let uploadStream = file.createWriteStream().on('error', function (err) {
            reject(err)
          }).on('finish', function ( /*file, path*/ ) {
            return file.getSignedUrl(config).then(function (data) {
              let url = data[0]
              resolve(url)
            })
          })

          let template = factura.templateCode || 'basic'
          let pdfDoc

          if (template == 'basic' && factura.tipoComprobante == 'I') {
            let pdfBuilder = require('./pdf-builder')
            pdfDoc = await pdfBuilder(factura, template)
            pdfDoc.pipe(uploadStream)
            pdfDoc.end()
          } else {
	          template = (factura.tipoComprobante == 'P') ? 'recibo_pago' : template
	          
            let request = require('request')
            let cfdiPdfUrl = `http://localhost:3000/exportables/pdf/${template}.pdf`

            debugger

            return request.post(cfdiPdfUrl,{
              json: {xml: `${factura.xmlDoc}`, json: factura}
            }).pipe(uploadStream)
          }
        }).catch(reject)
      } catch (e) {
        reject(e)
      }
    })
  }





  function buildAndSavePdf(facturaSnap){
    return buildPDF(facturaSnap).then(function(url){
      return facturaSnap.ref.update({ pdfUrl: url }).then(()=>{
        return url
      })
    })
  }




  function generateFilename(facturaSnap, cancelado) {
    let moment = require('moment-timezone')

    let accountRef = facturaSnap.ref.parent.parent
    let factura = facturaSnap.val()
    let sufix = (cancelado || isCancelada(factura)) ? '-cancelado' : ''
    // let timestamp = moment.utc(factura.fechaTimbrado).format('x')
    debugger

    filename = [
      'accounts', accountRef.key,
      'facturas', facturaSnap.key, `${factura.uuid}${sufix}`,
    ].join('/')

    return filename
  }




  function isCancelada(factura){
    return !!factura.fechaCancelado || factura.xmlCanceladoUrl
  }
}();
