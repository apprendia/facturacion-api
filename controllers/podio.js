const _ = require('lodash')
// const Promise = require('promise')
const podio = require(global.rootPath('config/podio'))


function verifyHook(params) {
  // params // { hook_id: '8176861', code: '08e886bc', type: 'hook.verify' }
  const client = podio.client()
  client.authObject = {}
  return client.request('POST', `hook/${params.hook_id}/verify/validate`, { code: params.code }, {})
}


exports.default = function(req, res, next) {
  const body = req.body, params = req.params

  switch (_.get(body, 'type')) {
    case 'hook.verify':
      return verifyHook(body)
        .then(()=> res.status(200).end())
        .catch((err)=> {
          console.log(new Error(err))
          res.status(500).end()
        })
  }

  if (_.get(params, 'app')){
    return podio.registerAllApps()
      .then(()=> next('route'))
      .catch((err)=> {
        console.log(new Error(err))
        res.status(500).end()
      })
  }

  return res.status(200).end()
}
