const db = require(global.rootPath("config/admin")).db
const admin = require(global.rootPath("config/admin")).app
const Promise = require('promise')
const request = require('request')
const elastic = require(global.rootPath("config/elastic"))


function findFactura(id, accountId) {
  function findById(id) {
    return db.ref('facturas').child(id).child('path').once('value').then(function(snap) {
      let path = snap.val()
      if (path) return path
      console.log('Not found by id', id)
      return Promise.reject('Not found by id')
    })
  }

  function findByUuid(uuid) {
    return db.ref('facturas').orderByChild('uuid').equalTo(uuid).limitToLast(1).once('value').then(function(snaps){
      let obj = snaps.val()
      if(!obj){
	      console.log('Not found by uuid', uuid)
	      return Promise.reject('Not found by uuid')
	     }

      let factura = obj[Object.keys(obj)[0]]
      return ((factura.path) ? factura.path : Promise.reject('Not found in account'))
    })
  }
  
  
  function findByElastic(id){
    return elastic.getSource({
      id: id,
      index: 'facturas',
      type: '_doc',
      _source: ['id', 'account']
    }).then(({body: {id, account}})=> db.ref('accounts').child(account).child('facturas').child(id).path )
  }

  function facturaFound(path) {
    if (path && `${path}`.length > 0) return db.ref(path).once('value')
    return Promise.reject('Required path attribute was not found')
  }
  
  if(accountId){
    return facturaFound(db.ref('accounts').child(accountId).child('facturas').child(id).path) 
  }

  return findById(id).catch(()=> findByUuid(id)).catch(()=> findByElastic(id)).then(facturaFound)
}

function encodeRFC5987ValueChars(str) {
  return encodeURIComponent(str).
  // Note that although RFC3986 reserves "!", RFC5987 does not,
  // so we do not need to escape it
  replace(/['()]/g, escape). // i.e., %27 %28 %29
  replace(/\*/g, '%2A').
  // The following are not required for percent-encoding per RFC5987,
  // so we can allow for a little better readability over the wire: |`^
  replace(/%(?:7C|60|5E)/g, unescape);
}


module.exports = function() {
  function xml(req, res){
    let id = req.params.id
    let accountId = req.query.accountId
    
    res.setHeader('Content-Type', 'text/xml')
    
    console.log('id ', id);
      
    return findFactura(id, accountId).then(async function(facturaSnap){
      let xmlUrl = facturaSnap.val().xmlUrl
      
      console.log('xmlUrl ', xmlUrl);
      
      function getXmlFromPac(){
        const Factura = require(global.rootPath('models/factura'))
        const retrieve = require(global.rootPath('services/pacs/edicom/retrieve'))
        const exportable = require(global.rootPath('exportable'))
        
        return Factura.load(facturaSnap).then(function(factura){
          return retrieve(factura).then((result) => {
            exportable.buildXML(facturaSnap, `${result.doc}`).then(function (url) {
              return facturaSnap.ref.update({ xmlUrl: url })
            })
            return res.send(`${result.doc}`)
          })
        })        
      }
      
      if(xmlUrl){
        return require('request').get(xmlUrl, {timeout: 30000},function(error, response, body){
          if(error){ return getXmlFromPac() }
          return res.send(body);
        })
      }
      
      return getXmlFromPac()
    })
  }
  
  function pdf(req, res) {
    
    const responseError = (status, msg = '') => {
      return res.status(Number(status) || 500).send(msg);
    };

    let id = req.params.id
    let accountId = req.query.accountId

    if (!id) return responseError(404)

    return findFactura(id, accountId).then(async function(facturaSnap) {
      try {
        const Factura = require(global.rootPath('models/factura'))
        
        
        let factura = facturaSnap.val()
        if(factura.fechaCancelado && false && !factura.canceladoCleaned){
          
          facturaSnap = await facturaSnap.ref.update({xmlUrl: null}).then(()=> facturaSnap.ref.once('value'))
          factura = await Factura.load(facturaSnap)
          
          const retrieve = require(global.rootPath('services/pacs/edicom/retrieve'))
          const exportable = require(global.rootPath('exportable'))
          
          await retrieve(factura).then(async (result) => {
            await exportable.buildXML(facturaSnap, `${result.doc}`, false).then(function (url) {
              return facturaSnap.ref.update({ xmlUrl: url, canceladoCleaned: `${new Date}` }).then(async function () {
                facturaSnap = await facturaSnap.ref.once('value')
              })
            })
          })
        }

        return Factura.load(facturaSnap).then(async function(factura) {
          let template = factura.templateCode || 'basic'
          let pdfDoc

          filename = encodeRFC5987ValueChars(`${factura.uuid}.pdf`)
//           res.setHeader('Content-disposition', `inline; filename="${filename}"`)
          res.setHeader('Content-type', 'application/pdf')

          if (['basic', 'old-basic'].includes(template) && ['E', 'I'].includes(factura.tipoComprobante)) {
            let pdfBuilder = require(global.rootPath('pdf-builder'))
            pdfDoc = await pdfBuilder(factura, 'basic')
            pdfDoc.pipe(res)
            pdfDoc.end()

          } else if (['N'].includes(factura.tipoComprobante)){
            const satPdfApi = require(global.rootPath('services/external/satpdf'))

            return satPdfApi.xmlTopdf(`${factura.xmlDoc}`, (buffer) => {
              res.send(buffer)
            });
            
          } else {
            debugger
            template = (factura.tipoComprobante == 'P') ? 'recibo_pago' : template
                        
            let request = require('request')
            let cfdiPdfUrl = `https://miscfdis-api.herokuapp.com/exportables/pdf/${template}.pdf`//`http://localhost:3000/exportables/pdf/${template}.pdf`

            return request.post(cfdiPdfUrl, {
              json: { xml: `${factura.xmlDoc}`, json: factura }
            }).pipe(res)
          }
        }).catch((e)=>{
          console.error(e.stack)          
          responseError(500, e.stack);
        })
      } catch (e) {
        console.error(e.stack)
        return responseError(500, e.stack)
      }
    }).catch((error) => {
      console.error(error.stack)
      responseError(404, error.stack)
    })
  }

  return {
    pdf: pdf,
    xml: xml
  }
}()
