const db = require(global.rootPath("config/admin")).db
const admin = require(global.rootPath("config/admin")).app
const elastic = require(global.rootPath("config/elastic"))
const request = require('request');

module.exports = function () {
  return {
    send
  }

  function send(req, res) {
    const webhookUrl = req.query.url;
    const payload = req.body;

    // console.log('> req.query', req.query, 'payload', JSON.stringify(payload));

    return request.post(
      webhookUrl,
      { json: payload, followAllRedirects: true, followOriginalHttpMethod: true },
      function (error, response, body) {
        if (error || (response && response.statusCode >= 400)) {
          console.error(`Error Webhook Response:`, error, response, response.statusCode)
          return res.status(400).json({ error, response });
        }

        // console.log('response', response, 'body', body);

        return res.status(200).json({});
      }
    );
  }
}()