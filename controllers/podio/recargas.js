const _ = require('lodash')
const Promise = require('promise')
const db = require(global.rootPath('config/admin')).db
const PodioItem = require(global.rootPath('utils/podio/item'))
const moment = require('moment-timezone')

const podio = require(global.rootPath('config/podio'))
let podioClient;


exports.default = function(req, res) {
  const body = req.body, params = req.params

  function handleRequest(){
      switch (_.get(body, 'type')) {
      case 'item.create':
        return create(params, body, res)
      case 'item.update':
      case 'file.change':
        return update(params, body, res)
      case 'item.delete':
        return destroy(params, body, res)
        // default:
        //   return res.status(200).end()
    }
  }

  podioClient = podio.client(body.appName || 'recargas')

  return podioClient.isAuthenticated()
    .then(()=> handleRequest())
    .then(()=> res.status(200).end())
    .catch((error) => {
      console.error(error)
      res.status(422).end()
    })
}







function create(params, body, res) {
  // console.log('create@podio#recargas')
  return podioClient.request('GET', `/item/${body.item_id}`).then(function(responseData) {
    const recargaPodio = new PodioItem(responseData)
    const accountId = recargaPodio.account || recargaPodio.accountid

    if (!accountId) {
      throw `Missing accountId: ${accountId}`
    }

    const recargaRef = db.ref(`accounts/${accountId}/recargas`).push()
    const payload = { fields: { firebaserecargaid: recargaRef.key } }


    if(!recargaPodio.date || !recargaPodio.date.start_utc){
      payload.fields.date = moment.utc().format('YYYY-MM-DD HH:MM:SS')
    }

    return podioClient.request('PUT', `/item/${recargaPodio.id}`, payload)
  })
}










function update(params, body, res) {
  console.log('update@podio#recargas')
  return podioClient.request('GET', `/item/${body.item_id}`).then(function(responseData) {
    const recargaPodio = new PodioItem(responseData)

    const recargaFirebaseId = recargaPodio.firebaserecargaid
    const accountId = recargaPodio.account || recargaPodio.accountid

    if (!accountId) {
      throw `Couldn't find recarga with params: recargaFirebaseId: ${recargaFirebaseId} and accountId: ${accountId}`
    }

    let fields = {}, recargaRef

    if(recargaFirebaseId){
      recargaRef = db.ref(`accounts/${accountId}/recargas/${recargaFirebaseId}`)
    } else {
      // Doesn't have `recargaFirebaseId` yet
      recargaRef = db.ref(`accounts/${accountId}/recargas`).push()
      fields = { ...fields, firebaserecargaid: recargaRef.key }
    }
    
    const attrs = buildRecargaAttrs(recargaPodio)
    
    return recargaRef.update(attrs).then(function(){
      return recargaRef.child('facturas').once('value').then(function(snap){
        if (Number(recargaPodio.firebasefacturascount) != snap.numChildren() || _.get(recargaPodio, 'actions.text') == 'Calcular ocupados'){
          fields = { 
            ...fields, 
            firebasefacturascount: snap.numChildren(),
            actions: null 
          }
        }

        if(!_.isEmpty(fields)) 
          return podioClient.request('PUT', `/item/${body.item_id}?hook=false`, { fields })
      })
    })
    .then(()=>{
			console.log('recargaPodio attrs', attrs)
	    if(attrs.onlyone === true){
		    return deactivateOthers(accountId, recargaFirebaseId)
	    }
    })
    
  })
}









function destroy(params, body, res) {
  // console.log('destroy@podio#recargas')

  let podioId = body.item_id
  db.ref('recargas').orderByChild('podioId').equalTo(podioId).once('child_added', function(recargaSnap) {
    if (recargaSnap.val() && recargaSnap.val().path) {
      return db.ref(recargaSnap.val().path).remove()
    }
  })

  return true
}









function buildRecargaAttrs(podioItem) {
  let attrs = {
    activo: true,
    creditos: Number(podioItem.ammount) || 0,
    fecha: podioItem.date ? Number(moment(podioItem.date.start_utc).format('x')) : null,
    pago: Number(podioItem.payed) || 0,
    podioId: `${podioItem.id}`
  }

  attrs.recurrent = (_.get(podioItem, 'recurrente.text') == 'True') ? true : null
  attrs.onlyone = (_.get(podioItem, 'disabledlast.text') == 'True') ? true : null

  return attrs
}


function deactivateOthers(accountId, recargaId){
  console.log('deactivateOthers', 'recargaId ', recargaId)

  let recargasRef = db.ref(`/accounts/${accountId}/recargas`).orderByKey()//.endAt(recargaId)

  return recargasRef.once('value').then(function(recargas) {
    let promises = []

    recargas.forEach(function(recarga) {
      if (recarga.key != recargaId/* && recarga.val().recurrent*/) {
        // console.log('recarga.key ', recarga.key)
        promises.push(recarga.ref.update({ activo: false, onlyone: null }))
      }

      promises.push(Promise.resolve())
    })

    return Promise.all(promises)
  })
}
