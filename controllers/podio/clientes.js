const _ = require('lodash')
const Promise = require('promise')
const podioConfig = require(global.rootPath('config/podio'))
const db = require(global.rootPath('config/admin')).db
const PodioItem = require(global.rootPath('utils/podio/item'))
const moment = require('moment-timezone')


let clientesApp;


exports.default = function (req, res) {
  const body = req.body, params = req.params

  // console.dir(body, {depth: null})
  // console.dir(params, {depth: null})

  function handleRequest() {
    switch (_.get(body, 'type')) {
      case 'item.create':
        return create(params, body, res)
      case 'item.update':
      case 'file.change':
        return update(params, body, res)
      case 'item.delete':
        return destroy(params, body, res)
      // default:
      //   return res.status(200).end()
    }
  }

  clientesApp = podioConfig.client(body.appName || 'clientes')

  return clientesApp.isAuthenticated()
    .then(()=> handleRequest())
    .then(()=> res.status(200).end())
    .catch((error) => {
      console.error(error)
      res.status(422).end()
    })
}


function create(params, body, res) { }


function update(params, body, res) {
  return clientesApp.request('GET', `/item/${body.item_id}`).then(function (responseData) {
    const clientePodio = new PodioItem(responseData)

    const accountId = clientePodio.accountid

    if (!accountId) {
      throw `Missing accountId: ${accountId}`
    }

    // console.log('acciones.text', _.get(clientePodio, 'acciones.text'))

    switch (_.get(clientePodio, 'acciones.text')) {
      case 'Asignar recarga mensual':
        return _createPodioRecarga(clientePodio);
    }
  })
}


function destroy(params, body, res) { }

// 
// So far is exclusive for Saeko
// 
function _createPodioRecarga(clientePodio) {
  const creditos = Number(_.get(clientePodio, 'recargas-mensuales-saeko')) || 0;
  let promise = Promise.resolve();

  // console.log('creditos > 0 ', creditos > 0)

  if (creditos > 0) {
    promise.then(()=> {
      const recargasApp = podioConfig.client('recargas-saeko')
      return recargasApp.isAuthenticated().then(()=>{
        return recargasApp.request('POST', `/item/app/${podioConfig.apps['recargas-saeko'].id}/`, {
          fields: {
            cliente: clientePodio.id,
            ammount: creditos,
            disabledlast: 2,
            recurrente: 2,
            date: { start: moment().format('YYYY-MM-DD HH:mm:ss') },
            metodo: "Recargar mensual automática"
          }
        })
      })
    })
  }

  return promise.then(()=> clientesApp.request('PUT', `/item/${clientePodio.id}?hook=false`, { fields: { acciones: null } }))
  
}