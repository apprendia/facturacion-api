const db = require(global.rootPath("config/admin")).db
const admin = require(global.rootPath("config/admin")).app
const elastic = require(global.rootPath("config/elastic"))
const request = require('request');
const Promise = require('promise')
const path = require('path')
const util = require('util')
const chdir = require('in-folder')
const _ = require('lodash')


module.exports = function () {
	return {
		timbrar,
		sign,
		validate,
		_timbrar
	}


	function sign(req, res) {
		const { key, xml } = req.body || {};
		if (!(key && xml)) {
			return res.status(400).json({ errors: "Missing one of require paramenters: key or xml" });
		}

		return _generateCadenaOriginal(xml).then(function (cadena) {
			let sello = _sign64(cadena, key)
			res.status(200).json({ cadena, sello })
		}).catch((e) => res.status(422).json({ errors: `${e}` }))
	}



	function validate(req, res) {
		const { xml } = req.body || {};

		if (!(xml)) {
			return res.status(400).json({ errors: "Missing one of require paramenters: xml" });
		}

		const promise = new Promise(async function (resolve, reject) {
			let xmlDoc = require('libxslt').libxmljs.parseXml(xml)
			let xsdDoc = await require(global.rootPath('services/cfdv33xsd'))

			chdir(global.rootPath('config/zfactu/sat'), function () {
				return new Promise(function (resolve, reject) {
					xsdDoc.validate(xmlDoc, function (err, validationErrors) {
						if (err || validationErrors) {
							return reject(err || validationErrors)
						}
						resolve(true)
					})
				})
					.then(resolve)
					.catch(reject)
			})
		});

		return promise
			.then(() => res.status(200).json({ valid: true }))
			.catch((e) => res.status(400).json({ valid: false, errors: util.inspect(e, {showHidden: false, depth: null}) }))
	}



	function timbrar(req, res) {
		try {
			const { xml, test: prueba } = req.body || {};
//			let { test: prueba } = req.params || {};
			const pac = require(global.rootPath('services/pacs/edicom'));

			if (!(xml)) {
				return res.status(400).json({ errors: "Missing one of require paramenters: xml" });
			}

			return pac.timbrar(xml, { prueba })
				.catch((e) => {
					return res.status(422).json({ 
            errors: `${_.get(e, 'cause.root.Envelope.Body.Fault.detail.fault.text') || e}` 
          });
				})
				.then((result) => res.status(200).json({ ...result, xml: `${result.xml}` }))

		} catch (e) {
			return res.status(500).json({ errors: e })
		}
	}


	function _timbrar(req, res) {
		const { id, accountId } = req.params

		console.log('id ', id, 'accountId ', accountId)

		return db.ref('accounts').child(accountId).child('facturas').child(id).once('value')
			.then((facturaSnap) => {
				// Factura found, search timbrarRequest
				const factura = facturaSnap.val()

				if (!factura) {
					return res.status(404).json({ errors: { code: 'factura.notFound', msg: 'La factura pudo no ser gurdada correctamente, intente de nuevo' } })
				}

				console.log('factura ', factura)
				console.log('facturaSnap.ref.path ', facturaSnap.ref.path.toString())

				return db.ref('timbrarRequests').orderByChild('factura').equalTo(id).once('value')
			})
			.then((timbrarRequestSnap) => {
				// TimbrarRequest found, init timbrado
				const timbrarRequest = timbrarRequestSnap.val()

				if (!timbrarRequest) {
					return res.status(404).json({ errors: { code: 'timbrarRequest.notFound', msg: 'La factura pudo no ser gurdada correctamente, intente de nuevo' } })
				}

				const onTimbrarRequestAdded = require(global.rootPath('workers/timbrar')).onTimbrarRequestAdded
				return onTimbrarRequestAdded(timbrarRequestSnap).then((a) => {
					console.log('rest timbrar success', a)
					if (!a) return res.status(422).json({})
					return res.status(200).json({})
				});
			})
			.catch((e) => {
				console.error(e)
				return res.status(422).json({ errors: { code: 'timbrar.unknownError', msg: 'La factura no pudo ser timbrada, contacte a soporte, NO intente timbrar nuevamente', payload: `${e}` } })
			})
	}



	function _generateCadenaOriginal(docStr) {
		return new Promise(function (resolve, reject) {
			try {
				let libxslt = require('libxslt')

				chdir(global.rootPath('config/zfactu/sat'), function () {
					return new Promise(function (resolve, reject) {
						libxslt.parseFile('cadenaoriginal_3_3.xslt', function (err, stylesheet) {
							if (err) { reject(err) } else {
								stylesheet.apply(docStr, function (err, result) {
									// process.chdir(path.resolve(rootPath))
									if (err) { reject(err) } else { resolve(result) }
								})
							}
						})
					}).then(resolve).catch(reject)
				})
			} catch (e) { reject(new Error(e)) }
		})
	}

	function _sign64(cadena, keyPem) {
		let forge = require('node-forge')
		let pki = forge.pki
		let privateKey = pki.privateKeyFromPem(keyPem)
		let digest = forge.md.sha256.create()

		digest.update(cadena, 'utf8')

		return forge.util.encode64(privateKey.sign(digest)).replace("\n", '');
	}
}()