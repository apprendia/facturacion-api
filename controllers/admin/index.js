// module.exports = function() {
//   return {
//     retrieveFacturasByDate,
//     cancelFactura
//   }
// }()

// function retrieveFacturasByDate(req, res, next){
//   const body = req.body, params = req.params
//   const retrieveByDate = require(global.rootPath('services/pacs/edicom/retrieve/byDate'))

//   return retrieveByDate(body)
//     .then((data)=> res.send(data))
//     .catch((err)=> res.status(500).send(`${err}`))
// }

// function cancelFactura(req, res, next){
//   const body = req.body, params = req.params
//   const cancelar = require(global.rootPath('services/pacs/edicom/cancelar'))

//   return cancelar(body)
//     .then((data)=> res.send(data))
//     .catch((err)=> res.status(500).send(`${err}`))
// }
const Promise = require('promise');

module.exports = function () {
  return {
    retrieveFacturasByDate,
    cancelFactura,
    bulkCancel
  }
}()

function retrieveFacturasByDate(req, res, next) {
  const body = req.body, params = req.params
  const retrieveByDate = require(global.rootPath('services/pacs/edicom/retrieve/byDate'))


  const parseString = require('xml2js').parseString
  const Promise = require('promise')

  let objectValueParser = function (xmlString) {
    return new Promise(function (resolve, reject) {
      const opts = {
        // async: true
      }

      parseString(xmlString, opts, function (err, result) {
        if (err) return reject(new Error(err))
        syncXmlUrl(xmlString)
        resolve(result)
      })
    }).catch(() => { })
  }

  /*if(body.rfc === 'HEHB9104145E6'){
	  return res.status(404).send('Not found');
  }*/

  return retrieveByDate({ ...body, objectValueParser })
    .then((data) => res.send(data))
    .catch((err) => res.status(500).send(`${err}`))
}

function cancelFactura(req, res, next) {
  const body = req.body, params = req.params
  const cancelar = require(global.rootPath('services/pacs/edicom/cancelar'))

  return cancelar(body)
    .then((data) => res.send(data))
    .catch((err) => res.status(500).send(`${err}`))
}

function bulkCancel(req, res, next) {
  const _ = require('lodash')
  const body = req.body, params = req.params

  const cancelar = require(global.rootPath('services/pacs/edicom/cancelar'))
  const retrieve = require(global.rootPath('services/pacs/edicom/retrieve'))
  const db = require(global.rootPath("config/admin")).db


  if (!_.isArray(body)) {
    return res.status(400).send(`An array of {uuid, rfc } objects is required`)
  }



  const promises = body.map((obj) =>
    new Promise(async function (resolve) {
      try {
        let { uuid, rfc, noCertificado, total, rfcReceptor } = obj || {};

        if (_.isNil(noCertificado) || _.isNil(total) || _.isNil(rfcReceptor)) {
          const { doc } = await retrieve({ emisor: { rfc }, uuid })

          noCertificado = doc.root().attr('NoCertificado').value()
          total = doc.root().attr('Total').value()
          rfcReceptor = doc.find('//cfdi:Comprobante/cfdi:Receptor', { cfdi: doc.root().namespace().href() })[0].attr('Rfc').value()
        }

        const certificado = (await db.ref('fiscalCertificados').child(noCertificado).once('value')).val()

        if (_.isNil(certificado)) {
          throw `CSD "${noCertificado}" was not found`
        }

        return cancelar({
          emisor: { rfc },
          receptor: { rfc: rfcReceptor },
          total,
          uuid,
          certificado
        })
          .then((data) => _.isNil(data) ? Promise.reject() : data)
          .then((data) => resolve({
            ...data,
            uuid,
            doc: `${data.doc}`
          }))
          .catch((error) => resolve({ uuid, error }))
      } catch (error) {
        console.error(error)
        return resolve({ error })
      }
    })
  )

  return Promise.all(promises)
    .then((data) => res.send(data))
    .catch((err) => res.status(500).send(`${err}`))
}




function syncXmlUrl(xmlString) {
  // Sync with factura
  const exportable = require(global.rootPath('exportable'))
  const sax = require('sax');
  const parser = sax.parser();
  const db = require(global.rootPath("config/admin")).db

  parser.onprocessinginstruction = function (instruction) {
    if (instruction.name === 'mcfactura')
      instruction.body.split(' ').map((i) => i.split('=')).forEach(([name, val]) => {
        switch (name) {
          case 'ref':
            db.ref(val.replace('https://contamc-facturacion.firebaseio.com/', ''))
              .once('value')
              .then((facturaSnap) => {
                if (!facturaSnap.val()) return false;

                const { xmlUrl } = facturaSnap.val();
                if (!xmlUrl)
                  exportable.buildXML(facturaSnap, xmlString).then(function (url) {
                    return facturaSnap.ref.update({ xmlUrl: url }).then(function () {
                      return facturaSnap.ref.once('value');
                    })
                  })
              });
        }
      })
  }
  parser.write(xmlString).close();
}
