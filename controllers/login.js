'use strict';

// Utils
let _ = require('underscore');
let Promise = require('promise');

// Firebase Admin
let admin = require("../config/admin").app;
let db = require("../config/admin").db;

// JWT
let r = require('jsrsasign');
let b64utoutf8 = r.b64utoutf8;
let JWS = r.jws.JWS;



let parseJWT = (sJWT) => {
  let headerObj = JWS.readSafeJSONString(b64utoutf8(sJWT.split(".")[0]));
  let payloadObj = JWS.readSafeJSONString(b64utoutf8(sJWT.split(".")[1]));
  return Object.assign(headerObj, payloadObj);
};

let verifyJWT = (sJWT, secret) => {
  let jwt = require('jsonwebtoken')
  /*
    return JWS.verifyJWT(sJWT, secret, {
      alg: ['HS256']
    });
  */


  try {
    return !!jwt.verify(sJWT, secret)
  } catch (err) {
    return false
  }
}



module.exports = {
  auth: function (req, res) {
    let access_code = req.body.access_code;

    let {
      uid,
      webhook_url
    } = parseJWT(access_code);

    if (!_.isString(uid) || _.isEmpty(uid)) {
      return res.status(400).send({
        error: 'Missing uid'
      })
    }

    db.ref("users").child(uid).once('value', function (snapshot) {
      let user = snapshot.val();

      if (!user) {
        return res.status(400).send({
          error: 'Bad uid'
        })
      }

      if (verifyJWT(access_code, user.access_token)) {
        admin.auth().createCustomToken(uid, {
          webhook_url: webhook_url
        }).then((customToken) => {
          return res.send({
            access_token: customToken
          })
        })
      } else {
        return res.status(401).send({
          error: 'Bad signature'
        });
      }
    });
  }
}
