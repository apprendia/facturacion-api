const db = require(global.rootPath("config/admin")).db
const admin = require(global.rootPath("config/admin")).app
const elastic = require(global.rootPath("config/elastic"))
const util = require('util')

module.exports = function() {
	return {
		setup,
		token
	}

	function token(req, res){
		const authToken = req.headers.authorization.split(' ')[1]
		const id = req.params.id

		if(!id) return res.status(400).send('User id required');

		return admin.auth().verifyIdToken(authToken)
			.catch(()=> res.status(403).send('Recent sign in required') )
			.then(async (decodedToken) => {
				const { user_id } = decodedToken

				if(id !== user_id) return res.status(403).send('Not authorized')

				return admin.auth().createCustomToken(user_id).then((token)=> res.status(200).send({token}))
			})
			.catch((e)=> {
				console.error(e);
				return res.status(500).send('Unknown error');
			})
	}
	
	function setup(req, res){
		const token = req.headers.authorization.split(' ')[1]
		const id = req.params.id

		return admin.auth().verifyIdToken(token)
			.catch(()=> res.status(403).send('Recent sign in required') )
			.then(async (decodedToken) => {
				const { user_id } = decodedToken
				
				if(!id) return res.status(400).send('User id required')
				//if(id !== user_id) return res.status(403).send('Not authorized')
				
				const user = (await db.ref('users').child(id).once('value')).val()
				let { account, email } = user
				
				//account = account.replace('-', '')
				
				// Setup user, role and aliases for user
				// - Aliases
				await elastic.indices.putAlias({
					index: 'facturas',
					name: `facturas_${account}`,
					body: {
						routing: `facturas_${account}`,
						is_write_index: true,
						filter: { term: { 'account.keyword': account } }
					}
				}).catch((e)=> console.log(JSON.stringify(e, null, 2)))
				
				await elastic.indices.putAlias({
					index: 'fiscal_receptores',
					name: `fiscal_receptores_${account}`,
					body: {
						routing: `fiscal_receptores_${account}`,
						is_write_index: true,
						filter: { term: { 'account.keyword': account } }
					}
				})
				
				// - Role
				await elastic.xpack.security.putRole({
					name: `account-${account}`,
					refresh: 'true',
					body: {
						cluster: ['all'],
						indices: [
							{
								names: [ `facturas_${account}`, `fiscal_receptores_${account}`, 'c_*' ],
								privileges: ['read', 'write']
							}
						],
					}
				})
				
				// - User
				await elastic.xpack.security.putUser({
					username: user_id,
					refresh: 'true',
					body: {
						password: user.access_token,
						roles: [ `account-${account}` ],
						metadata: {
							uid: user_id
						}
					}
				})
				
				return res.status(200).json({})
			})
			.catch((err)=> {
				console.log(JSON.stringify(err, null, 2))
				res.status(500).send(`${err}`) 
			})
	}
}()