module.exports = function () {
	global.Intl = require('intl')
	let Promise = require('promise')
	let objectPath = require("object-path")
	let name = require('../../../services/catalogoSat').getName
	let localDate = require('../../../utils/format-date').local
	let _ = require('underscore')
	
	_.mixin(require("underscore.string").exports());


	function moneyFormat(number, currency) {
		return new Intl.NumberFormat("es-MX", {
			style: "currency",
			currency: currency || "MXN"
		}).format(number || 0)
	}


	function replaceTipoProceso(clave){
		switch(clave){
			case '1':
				return 'Precampaña'
			case '2':
				return 'Ordinario'
			case '3':
				return 'Campaña'
			default:
				return clave
		}
	}


	async function facturaDetails(factura) {
		let obj = factura.get('emisor.fiscalDirecciones')
		debugger
		let emisorDireccion = (Object.values(obj).filter(function(dir){
			return dir.cp == factura.get('lugarExpedicion');
		})[0] || obj[Object.keys(obj)[0]]).direccion //obj[Object.keys(obj)[0]].direccion
		obj = factura.get('receptor.fiscalDirecciones')

		let receptorCP;
		try{
			receptorCP = _.rjust(`${obj[Object.keys(obj || {})[0]].cp}`, 5, '0')
		}catch(e){
			receptorCP = '';
		}
		let emisorCP = _.rjust(`${factura.get('lugarExpedicion')}`, 5, '0')
		let receptorDireccion = obj ? `${obj[Object.keys(obj)[0]].direccion} CP ${receptorCP}` : null


		let bodyDetails = [
					[{
							table: {
								widths: ['*'],
								border: [false, false, false, false],
								body: [

									[{
										text: [{
											text: `${factura.get('emisor.rfc')}\t`,
											fontSize: 8,
										}, `\n${factura.get('emisor.nombre')}\n`, `${factura.get('emisor.regimen')} ${await name('c_regimenfiscal', factura.get('emisor.regimen'))}`, `\n ${emisorDireccion} \n Código postal del lugar de expedición: ${emisorCP}`],
										margin: [0, 0, 0, 0]
									}],
									[{
										text: [{
											text: 'Facturado a:\n',
											bold: true
										}, {
											text: `${factura.get('receptor.rfc')}`,
											fontSize: 8,
										}, `\n${factura.get('receptor.nombre')}\n ${receptorDireccion || ''} \n `],
										margin: [0, 0, 0, 0]
									}]
								],
							},
							layout: 'noBorders'
						},
						{
							table: {
								widths: [60, '*'],
								body: [
									[{
										text: 'Tipo de comprobante\t',
										alignment: 'right',
										border: [false, false, false, false]
									}, {
										text: `${factura.get('tipoComprobante')}`,
										border: [false, false, false, false],
									}],
									[{
										text: 'Folio fiscal\t',
										alignment: 'right',
										border: [false, false, false, false]
									}, {
										text: `${factura.get('uuid', '')}`,
										border: [false, false, false, false],
									}],
									[{
										text: 'Serie y folio\t',
										alignment: 'right',
										border: [false, false, false, false]
									}, {
										text: `${factura.get('serie','')} ${factura.get('folio','')}`,
										border: [false, false, false, false],
									}],
									[{
										text: 'Emitido el\t',
										alignment: 'right',
										border: [false, false, false, false]
									}, {
										text: `${localDate(factura.get('fecha'), 'llll')}`,
										border: [false, false, false, false],
									}],
									[{
										text: 'Certificado el\t',
										alignment: 'right',
										border: [false, false, false, false]
									}, {
										text: `${localDate(factura.get('fechaTimbrado'), 'llll')}`,
										border: [false, false, false, false],
									}],
									[{
										text: 'Uso del CFDI\t',
										alignment: 'right',
										border: [false, false, false, false]
									}, {
										text: `${factura.get('usoCfdi')} ${await name('c_usocfdi', factura.get('usoCfdi')) }`,
										border: [false, false, false, false],
									}],
									[{
										text: 'Forma de pago\t',
										alignment: 'right',
										border: [false, false, false, false]
									}, {
										text: `${_.rjust(`${factura.get('formaPago.0') || ''}`, 2, '0')} ${factura.get('formaPago.0') && (await name('c_formapago', factura.get('formaPago.0', '')))}`,
										border: [false, false, false, false],
									}],
									[{
										text: 'Método de pago\t',
										alignment: 'right',
										border: [false, false, false, false]
									}, {
										text: `${factura.get('metodoPago')}`,
										border: [false, false, false, false],
									}],
									[{
										text: 'Moneda\t',
										alignment: 'right',
										border: [false, false, false, false]
									}, {
										text: `${factura.get('moneda')}`,
										border: [false, false, false, false],
									}]
								]
							},
							layout: {
								paddingLeft: function () {
									return 2
								},
								paddingBottom: function () {
									return 1
								},
								paddingTop: function () {
									return 2
								},
								paddingRight: function () {
									return 1
								}
							}
						},
					]
				]

		return {
			table: {
				widths: ['*', 220],
				margin: [0, 0, 0, 0],
				body: bodyDetails,
			},
			layout: 'noBorders',
		}
	}




	async function conceptoSection(concepto, opts) {
		concepto = objectPath(concepto || {})
		opts = opts || {}


		let conceptoSection = {
			table: {
				widths: [50, '*', 60, 60],
				body: [
					[{
							table: {
								headerRows: 1,
								widths: ['*'],
								body: [
									[{
										text: 'Cantidad',
										bold: true,
										alignment: 'center',
										style: 'tableHeader',
										border: [false, false, false, false],
										margin: [0, 0, 0, 0]
									}],
									[{
										text: `${concepto.get('cantidad')}`,
										alignment: 'center',
										border: [false, false, false, false],
										margin: [0, 0, 0, 0]
									}]
								]
							},
							border: [false, false, false, true],
							margin: [0, 0, 0, 0],
						},
						{
							border: [false, false, false, true],
							table: {
								margin: [0, 0, 0, 0],
								widths: ['auto', '*'],
								body: [
									[{
										text: 'Unidad',
										bold: true,
										border: [false, false, false, false],
										style: 'tableHeader',
										alignment: ''
									}, {
										text: 'Concepto',
										bold: true,
										border: [false, false, false, false],
										style: 'tableHeader'
									}],
									[{
										text: `${concepto.get('claveUnidad')} ${await name('c_claveunidad', concepto.get('claveUnidad'))}`,
										border: [false, false, false, false],
										alignment: ''
									}, {
										border: [false, false, false, false],
										text: `${concepto.get('clave')} ${await name('c_claveprodserv', concepto.get('clave'))}`
									}],
									[{
										text: 'Descripción',
										bold: true,
										border: [false, false, false, false],
										style: 'tableHeader',
										colSpan: 2
									}, {}],
									[{
										colSpan: 2,
										border: [false, false, false, false],
										text: `${concepto.get('descripcion')} ${(concepto.get('numeroPredial')) ? ('Numero Predial ' + concepto.get('numeroPredial')) : ''}`
									}, {}],
									[{
										text: `${concepto.get('iedu')?"Datos del alumno": ''}`,
										bold: true,
										border: [false, false, false, false],
										style: 'tableHeader',
										colSpan: 2
									}, {}],
									[{
										colSpan: 2,
										border: [false, false, false, false],
										text: `${(concepto.get('iedu'))?(`Nombre: ${concepto.get('iedu.nombreAlumno')}. CURP: ${concepto.get('iedu.curp')}. Nivel educativo: ${concepto.get('iedu.nivelEducativo')}. RVOE: ${concepto.get('iedu.autRVOE')}.`): ""}`
									}, {}],
								]
							},
							layout: {
								paddingLeft: function () {
									return 2;
								},
								paddingBottom: function () {
									return 2;
								},
								paddingTop: function () {
									return 0;
								},
								paddingRight: function () {
									return 2;
								}
							}
						},
						{
							table: {
								headerRows: 1,
								widths: ['*'],
								body: [
									[{
										text: 'P. Unitario',
										bold: true,
										alignment: 'center',
										border: [false, false, false, false],
									}],
									[{
										text: `${moneyFormat(concepto.get('valorUnitario'))}`,
										alignment: 'center',
										border: [false, false, false, false],
									}]
								]
							},
							border: [false, false, false, false],
						},
						{
							table: {
								headerRows: 1,
								widths: ['*'],
								body: [
									[{
										text: 'Importe',
										bold: true,
										alignment: 'center',
										border: [false, false, false, false],
									}],
									[{
										text: `${moneyFormat(concepto.get('importe'))}`,
										alignment: 'right',
										border: [false, false, false, false],
										margin: [0, 0, 0, 0]
									}]
								]
							},
							border: [false, false, false, false],
							margin: [0, 0, 0, 0]
						}
					],
				]
			},
			layout: {
				defaultBorder: [false, false, false, false],
				paddingLeft: function () {
					return 1;
				},
				paddingBottom: function () {
					return 1;
				},
				paddingTop: function () {
					return 1;
				},
				paddingRight: function () {
					return 1;
				}
			}
		}


		if (opts.desgloseImpuestos) {
			let hasDescuento = objectPath(concepto, 'descuento', 0) > 0
			if (hasDescuento) {
				objectPath.push(
					conceptoSection,
					'table.body', [{
							border: [false, false, false, false],
							text: ''
						},
						{
							colSpan: 2,
							text: 'Descuento',
							alignment: 'right',
							border: [false, true, false, false]
						},
						{},
						{
							text: `${moneyFormat(concepto.get('descuento'))}`,
							alignment: 'right',
							border: [false, true, false, false]
						}
					]
				)
			}

			concepto.get('impuestos', []).forEach(function (impuesto, index) {
				//console.log(impuesto)

				let textImpuesto = (impuesto.tipoFactor=='Exento')?`${impuesto.impuestoLabel} Exento`: `${impuesto.impuesto} ${impuesto.impuestoLabel} Tasa: ${impuesto.tasaCuota}% (Base ${moneyFormat(impuesto.base)})`;

				objectPath.push(
					conceptoSection,
					'table.body', [{
							border: [false, false, false, false],
							text: ''
						},
						{
							colSpan: 2,
							text: `Impuesto ${textImpuesto}`,
							alignment: 'right',
							border: [false, !hasDescuento && index == 0, false, false]
						},
						{},
						{
							text: `${moneyFormat(impuesto.importe)}`,
							alignment: 'right',
							border: [false, !hasDescuento && index == 0, false, false]
						}
					]
				)
			})
		}

		return conceptoSection
	}





	async function render(factura, defaultDd) {
		factura = objectPath(factura || {})
		defaultDd = defaultDd || {}

		let dd = _.extend(defaultDd, {
			styles: {},
			pageMargins: [30, 40, 30, 40],
			pageSize: 'LETTER',
			defaultStyle: {
				font: 'HelveticaNeue',
				fontSize: 7,
				color: '#222'
			},
			content: []
		})


		if (factura.get('fechaCancelado') || factura.get('xmlCancelado')) {
			dd.watermark = {
				text: 'CANCELADO',
				color: 'red',
				opacity: 0.3,
				bold: true,
				italics: false
			}
		} else if (factura.get('prueba')) {
			dd.watermark = {
				text: 'PRUEBA',
				color: 'black',
				opacity: 0.3,
				bold: true,
				italics: false
			}
		} else if (factura.get('tipoComprobante') === 'E'){
			dd.watermark = {
				text: 'EGRESO',
				color: 'black',
				opacity: 0.3,
				bold: true,
				italics: false
			}			
		}


		if(dd.images && dd.images.logo){
		      try{
			dd.content.push({image: 'logo', fit: [150, 63], alignment: 'left'})
			}catch(e){ console.log('dd.images: ', dd.images); }
		}


		if(factura.get('emisor.nombreComercial')){
			dd.content.push({text: `${factura.get('emisor.nombreComercial')}`, alignment: 'left', size: 12, fontSize: 12})
		}



		dd.content.push({
			text: '\n\n'
		})

		dd.content.push(await facturaDetails(factura))

		dd.content.push({
			text: '\n\n'
		})

/*
		await Promise.all(
			_.sortBy(factura.get('conceptos'), (concepto)=>{
				return concepto.order;
			}).map(async function (concepto) {
				return dd.content.push(await conceptoSection(concepto, {
					desgloseImpuestos: factura.get('conceptos').length >= 1
				}))
			})
		)
*/

		if(factura.get('cfdiRelacionados.length')){
			dd.content.push({ text: 'CFDIs Relacionados\n', bold: true })
				
			await Promise.all(
				factura.get('cfdiRelacionados').map(async (obj)=>{
					cfdiRelacionado = objectPath(obj || {})
			
					return [
						{ text: `${cfdiRelacionado.get('tipoRelacion')} ${await name('c_tiporelacion',cfdiRelacionado.get('tipoRelacion'))}` },
						{ text: cfdiRelacionado.get('uuid') },
					]
				})
			).then((sections)=>{
				dd.content.push({
					table: {
						headerRows: 1,
						widths: [100, 180],
						body: [
							[
								{text: 'Tipo de Relación', bold: true, alignment: 'left', style: 'tableHeader', border: [false, false, false, false], margin: [0, 0, 0, 0] },
								{text: 'Folio Fiscal', bold: true, alignment: 'center', style: 'tableHeader', border: [false, false, false, false], margin: [0, 0, 0, 0] },
							],
							...sections
						]
					}
				})
			})
			
			dd.content.push({ text: '\nConceptos\n', bold: true })
		}

		await Promise.all(
			factura.get('conceptos').map(async function (concepto) {
				return [concepto, await conceptoSection(concepto, {
					desgloseImpuestos: factura.get('conceptos').length >= 1
				})]
			})
		).then((promises)=>{
			_.sortBy(promises, ([concepto, section])=>{
				return concepto.order
			}).forEach(([concepto, section])=>{
				dd.content.push(section)
			})
		})



		dd.content.push({
			text: '\n'
		})



		dd.content.push({
			table: {
				widths: ['*'],
				body: [
					[{
						text: `Subtotal: ${moneyFormat(factura.get('subtotal'))}`,
						alignment: 'right',
						border: [false, false, false, false]
					}]
				]
			}
		})


		if (factura.get('descuento', 0) > 0) {
			dd.content.push({
				table: {
					widths: ['*'],
					body: [
						[{
							text: `Descuento: ${moneyFormat(factura.get('descuento'))}`,
							alignment: 'right',
							border: [false, false, false, false]
						}]
					]
				}
			})
		}


		dd.content.push({
			text: '\n'
		})


		function generateResumenImpuestos() {
			let resumenImpuestos = {
				table: {
					widths: ['*', 55],
					body: []
				}
			}

			function sumaImpuestos(array) {
				return _.reduce(array, (sum, item) => {
					return item.importe + sum;
				}, 0);
			}

			let retenidosLocales = factura.get('impuestosLocales', []).filter((impuesto) => impuesto.tipo == 1)
			if (Object.keys(factura.get('impRetenidosGrouped')).length > 0 || retenidosLocales.length > 0) {
				objectPath.push(resumenImpuestos, 'table.body', [{
						colSpan: 2,
						text: 'Resumen de impuestos Retenidos',
						fillColor: '#eee',
						border: [false, false, false, false],
					},
					{}
				])

				_.each(factura.get('impRetenidosGrouped'), function (arr, key) {
					let first = arr[0]

					objectPath.push(resumenImpuestos, 'table.body', [{
							text: `${first.impuestoLabel} ${first.tasaCuota}%`,
							border: [false, false, false, false],
							alignment: 'right'
						},
						{
							text: `${moneyFormat(sumaImpuestos(arr))}`,
							alignment: 'right',
							border: [false, false, false, false]
						}
					])
				})

				if (retenidosLocales) {
					_.each(retenidosLocales, function (impuesto, key) {
						if (impuesto.tipo != 1) { return }

						objectPath.push(resumenImpuestos, 'table.body', [{
								text: `${impuesto.impuesto} ${impuesto.tasaCuota}%`,
								border: [false, false, false, false],
								alignment: 'right'
							},
							{
								text: `${moneyFormat(impuesto.importe)}`,
								alignment: 'right',
								border: [false, false, false, false]
							}
						])
					})
				}
			}

			let trasladosLocales = factura.get('impuestosLocales', []).filter((impuesto) => impuesto.tipo == 2)
			if (Object.keys(factura.get('impTrasladadosGrouped')).length > 0 || trasladosLocales.length > 0) {
				objectPath.push(resumenImpuestos, 'table.body', [{
						colSpan: 2,
						text: 'Resumen de impuestos Trasladados',
						fillColor: '#eee',
						border: [false, false, false, false],
					},
					{}
				])

				_.each(factura.get('impTrasladadosGrouped'), function (arr, key) {
					let first = arr[0]

					let textImpuesto = (first.tipoFactor=='Exento')?`${first.impuestoLabel} Exento`: `${first.impuestoLabel} ${first.tasaCuota}%`;

					objectPath.push(resumenImpuestos, 'table.body', [{
							text: textImpuesto,
							border: [false, false, false, false],
							alignment: 'right'
						},
						{
							text: `${moneyFormat(sumaImpuestos(arr))}`,
							alignment: 'right',
							border: [false, false, false, false]
						}
					])
				})


				if (trasladosLocales) {
					_.each(trasladosLocales, function (impuesto, key) {
						if (impuesto.tipo != 2) { return }

						objectPath.push(resumenImpuestos, 'table.body', [{
								text: `${impuesto.impuesto} ${impuesto.tasaCuota}%`,
								border: [false, false, false, false],
								alignment: 'right'
							},
							{
								text: `${moneyFormat(impuesto.importe)}`,
								alignment: 'right',
								border: [false, false, false, false]
							}
						])
					})
				}
			}

			if (objectPath.get(resumenImpuestos, 'table.body').length > 0) {
				objectPath.push(dd, 'content', resumenImpuestos)
			}
		}
		generateResumenImpuestos()


		dd.content.push({
			text: '\n'
		})

		let totalBody = [
					[{
							text: `${ Number(factura.get('total').toFixed(2)).toWords({symbol: factura.get('moneda')}) }`,
							alignment: 'right',
							border: [false, false, false, false],
							margin: [0, 3, 0, 0]
						},
						{
							text: 'Total',
							alignment: 'right',
							bold: true,
							border: [false, false, false, false],
							fontSize: 10
						},
						{
							text: `${moneyFormat(factura.get('total'))}`,
							alignment: 'right',
							border: [false, false, false, false],
							fontSize: 10
						}
					],
				]




			if(factura.get('ine')){
				debugger
				totalBody.push([{ text: '\n\n', colSpan: 3, border: [false, false, false, false],  }])
				totalBody.push([{ text: 'Complemento del INE', bold: true, colSpan: 3  }])
				totalBody.push([{ text: `Tipo de Proceso: ${replaceTipoProceso(factura.get('ine.tipoProceso'))}. Tipo de Comité: ${factura.get('ine.tipoComite') || '- - -'}. Clave de Contabilidad: ${factura.get('ine.idContabilidad') || '- - -'}`, colSpan: 3 }])

				factura.get('ine.entidades').forEach(function(entidad){
					totalBody.push([{ text: `Entidad: ${entidad.claveEntidad}. Ámbito: ${entidad.ambito}. Claves de contabilidad: ${entidad.contabilidades.map(function(c){ return c.idContabilidad }).join(', ')}`, colSpan: 3 }])
				})
		}



		dd.content.push({
			table: {
				widths: ['*', 'auto', 'auto'],
				body: totalBody
			}
		})



		dd.content.push({
			text: '\n\n\n'
		})


		if (factura.get('observaciones')){
			dd.content.push({
				text: factura.get('observaciones'),
			})	
	
			dd.content.push({
				text: '\n\n\n'
			})
		}


		dd.content.push({
			table: {
				widths: '*',
				body: [
					[{
						text: `No. Certificado: ${factura.get('certificado.numero')}`
					}, {
						text: `RFC Proveedor de Certificación: ${factura.get('rfcProvCertif')}`
					}, {
						text: `No. Certificado SAT: ${factura.get('noCertificadoSAT')}`
					}],

					[{
						colSpan: 3,
						text: `Cadena Original: ${factura.get('cadenaOriginalSat',' ').match(/(.|[\r\n]){1,100}/g).join('\n')}`
					}, {}],
					[{
						colSpan: 3,
						text: `Sello digital: ${factura.get('selloCFD',' ').match(/(.|[\r\n]){1,100}/g).join('\n')}`
					}, {}],
					[{
						colSpan: 3,
						text: `Sello del SAT: ${factura.get('selloSAT', ' ').match(/(.|[\r\n]){1,100}/g).join('\n')}`
					}, {}]
				]
			},
			layout: {
				hLineWidth: function (i, node) {
					return (i === 0 || i === node.table.body.length) ? 1.5 : 1;
				},
				vLineWidth: function (i, node) {
					return (i === 0 || i === node.table.widths.length) ? 1.5 : 1;
				},
				hLineColor: function (i, node) {
					return (i === 0 || i === node.table.body.length) ? '#333' : '#fff';
				},
				vLineColor: function (i, node) {
					return (i === 0 || i === node.table.widths.length) ? '#333' : '#fff';
				}
			}
		})



		dd.content.push({
			text: '\n'
		})



		dd.content.push({
			text: 'Este documento es una representación impresa de un CFDI',
			color: '#333',
			bold: true,
			alignment: 'center'
		})
		dd.content.push({image: 'qr', fit: [200, 120], alignment: 'center'})



		return dd;
	}



	return {
		render: render
	}
}();
