'use strict';

// Utils
const _ = require('underscore')
const Promise = require('promise')
const moment = require('moment-timezone')

// XML
// const builder = require('xmlbuilder');

// Firebase Admin
const admin = require("../config/admin").app
const db = require("../config/admin").db

var Decimal = require('decimal.js-light')
Decimal.set({
  precision: 24,
  rounding: 4
})


module.exports = function (factura) {
  let xml = require('xmlbuilder').create('cfdi:Comprobante', {
    encoding: 'utf-8'
  })
  
  try{
	  xml.instructionBefore('mcfactura',`fid="${factura.id || factura.key}" ref="${factura.ref.toString()}" eid="${factura.webhookUrl}"`)
  } catch(e){
	  console.error('Error building addenda')
  }
  
  
  let xmlComplemento
  if (!factura.decimals) factura.decimals = 2


  function addComplemento() {
    xmlComplemento = xmlComplemento || xml.ele('cfdi:Complemento')
    return xmlComplemento
  }

  function toDecimals(number) {
    if (!number) number = 0
    return (factura.coinDecimals) ? toMoneyDecimals(number) : toMaxDecimals(number)
  }

  function toMoneyDecimals(number) {
	  number = number || 0
    return number.toFixed(factura.decimals)
    // return NumberFormat("en-US", {currency: factura.moneda, maximumFractionDigits: factura.decimals}).format(number)
  }

  function toMaxDecimals(number) {
    if (!number) number = 0
    return Decimal(number).todp(6).toNumber()
  }

  function fixDescripcion(descripcion) {
    return `${descripcion || ''}`.substring(0, 999)
  }

  const NumberFormat = Intl.NumberFormat.bind(this)

  // Init cfdi:Comprobante
  xml.att({
    'xmlns:cfdi': "http://www.sat.gob.mx/cfd/3",
    'xmlns:xsi': "http://www.w3.org/2001/XMLSchema-instance",
    'xmlns:implocal': "http://www.sat.gob.mx/implocal",
    'xmlns:ine': "http://www.sat.gob.mx/ine",
    'xmlns:pago10': "http://www.sat.gob.mx/Pagos",
//     'xmlns:mcfactura': "http://www.sat.gob.mx/mcfactura",
    'xsi:schemaLocation': "http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd http://www.sat.gob.mx/implocal http://www.sat.gob.mx/sitio_internet/cfd/implocal/implocal.xsd http://www.sat.gob.mx/ine http://www.sat.gob.mx/sitio_internet/cfd/ine/ine11.xsd http://www.sat.gob.mx/Pagos http://www.sat.gob.mx/sitio_internet/cfd/Pagos/Pagos10.xsd",
    Version: "3.3",
  });
  //console.log("factura-xml things")



  // Build Factura Attrs
  xml.att({
    LugarExpedicion: ('' + factura.lugarExpedicion).rjust(5, '0'),
    TipoDeComprobante: factura.tipoComprobante,
    Total: ['E', 'I'].includes(factura.tipoComprobante) ? toMoneyDecimals(factura.total) : factura.total,
    Moneda: factura.moneda,
    SubTotal: ['E', 'I'].includes(factura.tipoComprobante) ? toMoneyDecimals(factura.subtotal) : factura.subtotal,
    Fecha: moment(factura.fecha).tz('America/Mexico_City').format('YYYY-MM-DDTHH:mm:ss'),

    Certificado: '',
    NoCertificado: '',
    Sello: ''
  })

  //if (!factura.hideSerieFolioXml) {
    if (factura.serie) {
      xml.att({
        Serie: factura.serie
      })
    }

    if (factura.folio) {
      xml.att({
        Folio: factura.folio
      })
    }
  //}


  if (factura.formaPago) {
    let fixed = factura.formaPago.map((i) => ('' + i).rjust(2, '0'));
    xml.att({
      FormaPago: fixed[0]
    })
  }

  if (factura.condicionesPago) {
    xml.att({
      CondicionesDePago: factura.condicionesPago
    })
  }

  if (factura.metodoPago && ['E', 'I'].includes(factura.tipoComprobante)) {
    xml.att({
      MetodoPago: factura.metodoPago
    })
  }

  if (factura.condicionesPago) {
    xml.att({
      CondicionesDePago: factura.condicionesPago
    })
  }

  if (Number(factura.descuento) > 0) {
    xml.att({
      Descuento: factura.descuento
    })
  }

  if (factura.tipoCambio) {
    xml.att({
      TipoCambio: factura.tipoCambio || '1'
    })
  }

  if (factura.confirmacion) {
    xml.att({
      Confirmacion: factura.confirmacion
    })
  }
  
  
  
  
  
    // Build CfdiRelacionados Attrs
  if(factura.cfdiRelacionados && factura.cfdiRelacionados.length > 0){
	  const grouped = _.groupBy(factura.cfdiRelacionados, 'tipoRelacion');
	  
	  _.each(grouped, (value, key)=>{
		  if(!key) return
		  
		  const cfdiRelacionadosNode = xml.ele('cfdi:CfdiRelacionados', {
			  TipoRelacion: key
		  });
		  
		  value.forEach((cfdiRelacionado)=>{
			  if(!cfdiRelacionado.uuid) return
			  
			  if(`${cfdiRelacionado.uuid}`.length > 0){		  	  
				  let obj = {
					  UUID: cfdiRelacionado.uuid
				  }
				  cfdiRelacionadosNode.ele('cfdi:CfdiRelacionado', obj);
			  }
		  })
	 })
  }
  







  // Build Emisor Attrs
  let emisor = factura.emisor
  if (emisor) {
    let obj = {
      Rfc: emisor.rfc,
      RegimenFiscal: emisor.regimen
    }

    if (emisor.nombre) {
      obj.Nombre = `${emisor.nombre}`.replace(/\./gi, '').trim()
    }

    xml.ele('cfdi:Emisor', obj)
  }








  // Build Receptor Attrs
  if (factura.receptor) {
    let receptor = factura.receptor
    let obj = {
      Rfc: receptor.rfc,
      UsoCFDI: factura.usoCfdi
    }

    if (receptor.nombre) {
      obj.Nombre = `${receptor.nombre}`.replace(/\./gi, '').trim()
    }

    if (receptor.residenciaFiscal) {
      obj.ResidenciaFiscal = receptor.residenciaFiscal
    }

    if (receptor.numRegistro) {
      obj.NumRegIdTrib = receptor.numRegistro
    }

    xml.ele('cfdi:Receptor', obj)
  }









  // Build Concepto Attrs
  let impuestosFromConceptos = {};
  let conceptosNode = xml.ele('cfdi:Conceptos')



  factura.conceptos.forEach((concepto) => {


    let obj = {
      ClaveProdServ: `${concepto.clave}`.rjust(8, '0'),
      Cantidad: concepto.cantidad,
      ClaveUnidad: concepto.claveUnidad,
      Descripcion: fixDescripcion(concepto.descripcion),
      ValorUnitario: ['E', 'I'].includes(factura.tipoComprobante) ? toDecimals(concepto.valorUnitario) : concepto.valorUnitario,
      Importe: ['E', 'I'].includes(factura.tipoComprobante) ? toMoneyDecimals(concepto.importe) : concepto.importe
    }

    if (concepto.noIdentificacion) {
      obj.NoIdentificacion = concepto.noIdentificacion
    }
    if (concepto.unidad) {
      obj.Unidad = concepto.unidad
    }
    if (Number(concepto.descuento) > 0) {
      obj.Descuento = toMoneyDecimals(concepto.descuento)
    }

    let conceptoNode = conceptosNode.ele('cfdi:Concepto', obj);


    if ((concepto.impuestos || []).length > 0) {
      let impuestosNode = conceptoNode.ele('cfdi:Impuestos');
      let trasladosNode;
      let retencionesNode;

      concepto.impuestos.forEach(function (impuesto) {
        if (impuesto.local) { return }
        let obj;

        if (impuesto.tipoFactor == "Exento") {
          obj = {
            Base: impuesto.base,
            Impuesto: impuesto.impuesto,
            TipoFactor: impuesto.tipoFactor,
          };

        } else {
          obj = {
            Base: toDecimals(impuesto.base),
            Impuesto: impuesto.impuesto,
            TipoFactor: impuesto.tipoFactor,
            TasaOCuota: (impuesto.tasaCuota / 100).toFixed(6),
            Importe: toDecimals(impuesto.importe)
          };


        }



        switch (impuesto.tipo) {
          case 1:
            // Retencion
            retencionesNode = retencionesNode || impuestosNode.ele('cfdi:Retenciones');
            retencionesNode.ele('cfdi:Retencion', obj);
            break;
          case 2:
            // Traslado
            trasladosNode = trasladosNode || impuestosNode.ele('cfdi:Traslados');
            trasladosNode.ele('cfdi:Traslado', obj);
            break;
        }
        impuestosFromConceptos[impuesto.tipo] = impuestosFromConceptos[impuesto.tipo] || [];
        impuestosFromConceptos[impuesto.tipo].push(impuesto);

      })
    }

    if (concepto.numeroPredial) {
      conceptoNode.ele('cfdi:CuentaPredial', {
        Numero: concepto.numeroPredial
      })
    }

    if ((concepto.partes || []).length > 0) {
      concepto.partes.forEach((parte) => {
        let obj = {
          ClaveProdServ: parte.clave,
          Cantidad: parte.cantidad,
          Descripcion: parte.descripcion
        }

        if (parte.valorUnitario) {
          obj.ValorUnitario = parte.valorUnitario
        }
        if (parte.importe) {
          obj.Importe = toMoneyDecimals(parte.importe)
        }
        if (parte.unidad) {
          obj.Unidad = parte.unidad
        }

        conceptoNode.ele('cfdi:Parte', obj);
      });
    }



    // Complementos de concepto
    let compConceptoNode;

    if (concepto.iedu) {
      xml.att({
        'xmlns:iedu': "http://www.sat.gob.mx/iedu"
      });

      compConceptoNode = compConceptoNode || conceptoNode.ele('cfdi:ComplementoConcepto')

      let iedu = concepto.iedu
      compConceptoNode.ele('iedu:instEducativas', {
        version: '1.0',
        nombreAlumno: iedu.nombreAlumno,
        CURP: iedu.curp,
        nivelEducativo: iedu.nivelEducativo,
        autRVOE: iedu.autRVOE,
        // rfcPago: '', // opcional
      })
    }
  })






  // Build Impuestos Attrs
  // If any impuestos where collected from conceptos
  if (_.values(impuestosFromConceptos).length != 0) {

    let impuestosNode = xml.ele('cfdi:Impuestos'),
      totalRetenciones, totalTraslados;

    let retenciones = impuestosFromConceptos['1'];
    if (!!retenciones) {
      let retencionesNode = impuestosNode.ele('cfdi:Retenciones');
      let retencionesGrouped = {};
      retenciones.forEach((retencion) => {
        let key = [retencion.impuesto, retencion.tasaCuota, retencion.tipoFactor].join('_');

        retencionesGrouped[key] = retencionesGrouped[key] || [];
        retencionesGrouped[key].push(retencion);

        totalRetenciones = totalRetenciones || 0;
        let importe = retencion.importe || 0
        totalRetenciones += parseFloat(toMoneyDecimals(importe));
      });

      _.each(retencionesGrouped, (arr, key) => {
        let firstElement = arr[0];
        let suma = _.reduce(arr, (sum, item) => {
          let importe = (item.importe) ? parseFloat(toMoneyDecimals(item.importe)) : 0
          let sumNum = parseFloat(sum);
          return importe + sumNum;
        }, 0);

        retencionesNode.ele('cfdi:Retencion', {
          Impuesto: firstElement.impuesto,
          Importe: toDecimals(suma)
        });
      });
    }

    let traslados = impuestosFromConceptos['2'];
    if (!!traslados) {
      let trasladosNode = impuestosNode.ele('cfdi:Traslados');
      let trasladosGrouped = {};
      traslados.forEach((traslado) => {
        let key = [traslado.impuesto, traslado.tasaCuota, traslado.tipoFactor].join('_');

        trasladosGrouped[key] = trasladosGrouped[key] || [];
        trasladosGrouped[key].push(traslado);

        totalTraslados = totalTraslados || 0;
        let importe = traslado.importe || 0
        totalTraslados += parseFloat(toMoneyDecimals(importe)) || 0;
      });

      _.each(trasladosGrouped, (arr, key) => {
        let firstElement = arr[0];
        let suma = _.reduce(arr, (sum, item) => {

          let importe = (item.importe) ? parseFloat(toMoneyDecimals(item.importe)) : 0
          let sumNum = parseFloat(sum);
          return importe + sumNum;
        }, 0);

        let tasaCuota = (firstElement.tasaCuota) ? (firstElement.tasaCuota / 100).toFixed(6) : (0).toFixed(6)
        let obj = {
          Impuesto: firstElement.impuesto,
          TipoFactor: firstElement.tipoFactor,
          TasaOCuota: tasaCuota,
          Importe: toDecimals(suma) || toDecimals(0)
        }

        trasladosNode.ele('cfdi:Traslado', obj);
      });
    }

    if (totalRetenciones) {
      // //console.log(toMoneyDecimals(totalRetenciones))
      impuestosNode.att('TotalImpuestosRetenidos', toMoneyDecimals(totalRetenciones));
    }
    if (totalTraslados || totalTraslados == 0) {
      ////console.log("totalTraslados: ", toMoneyDecimals(totalTraslados))
      impuestosNode.att('TotalImpuestosTrasladados', toMoneyDecimals(totalTraslados));
    }
  }


  // 
  // Complementos
  // 

  // Impuestos Locales
  if (factura.impuestosLocales && factura.impuestosLocales.length > 0) {
    debugger
    addComplemento()

    let impuestosLocalesNode = xmlComplemento.ele('implocal:ImpuestosLocales', {
      version: '1.0',
    })

    let retencionesLocales = [],
      trasladosLocales = []

    factura.impuestosLocales.forEach(function (impuesto) {
      switch (impuesto.tipo) {
        case 1:
          // Retencion
          impuestosLocalesNode.ele('implocal:RetencionesLocales', {
            ImpLocRetenido: impuesto.impuesto,
            TasadeRetencion: toMoneyDecimals(impuesto.tasaCuota),
            Importe: toMoneyDecimals(impuesto.importe)
          })
          retencionesLocales.push(impuesto)
          break
        case 2:
          // Traslado
          impuestosLocalesNode.ele('implocal:TrasladosLocales', {
            ImpLocTrasladado: impuesto.impuesto,
            TasadeTraslado: toMoneyDecimals(impuesto.tasaCuota),
            Importe: toMoneyDecimals(impuesto.importe)
          })
          trasladosLocales.push(impuesto)
          break
      }
    })

    impuestosLocalesNode.att('TotaldeRetenciones', toMoneyDecimals(retencionesLocales.reduce((a, b) => a + (b.importe || 0), 0)))
    impuestosLocalesNode.att('TotaldeTraslados', toMoneyDecimals(trasladosLocales.reduce((a, b) => a + (b.importe || 0), 0)))
  }
  //console.log("end of factura-xml things")


  // INE
  if (factura.ine) {
    let ineNode = addComplemento().ele('ine:INE'), ine = factura.ine

    if (ine.version) ineNode.att('Version', ine.version)
    if (ine.tipoProceso) ineNode.att('TipoProceso', ine.tipoProceso)
    if (ine.tipoComite) ineNode.att('TipoComite', ine.tipoComite)
    if (ine.idContabilidad) ineNode.att('IdContabilidad', Number(ine.idContabilidad))

    if (ine.entidades) {
      ine.entidades.forEach(function (entidad) {
        let entidadNode = ineNode.ele('ine:Entidad')

        if (entidad.claveEntidad) entidadNode.att('ClaveEntidad', entidad.claveEntidad)
        if (entidad.ambito) entidadNode.att('Ambito', entidad.ambito)

        if (entidad.contabilidades) {
          entidad.contabilidades.forEach(function (contabilidad) {
            let contabilidadNode = entidadNode.ele('ine:Contabilidad')
            if (contabilidad.idContabilidad) contabilidadNode.att('IdContabilidad', Number(contabilidad.idContabilidad))
          })
        }
      })
    }
  }


  // Recepcion Pagos
  if (factura.recepcionPago) {
    let reciboPagoNode = addComplemento().ele('pago10:Pagos'), reciboPago = factura.recepcionPago

    if (reciboPago.version) reciboPagoNode.att('Version', reciboPago.version)

    if (reciboPago.pagos) {
      reciboPago.pagos.forEach(function (pago) {
        let pagoNode = reciboPagoNode.ele('pago10:Pago')

        if (pago.fechaPago) pagoNode.att('FechaPago', moment(pago.fechaPago).tz('America/Mexico_City').format('YYYY-MM-DDTHH:mm:ss'))
        if (pago.formaDePagoP) pagoNode.att('FormaDePagoP', `${pago.formaDePagoP}`.rjust(2, '0'))
        if (pago.monedaP) pagoNode.att('MonedaP', pago.monedaP)
        if (pago.tipoCambioP) pagoNode.att('TipoCambioP', pago.tipoCambioP)
        if (pago.monto) pagoNode.att('Monto', pago.monto)


        if (pago.rfcEmisorCtaOrd) pagoNode.att('RfcEmisorCtaOrd', pago.rfcEmisorCtaOrd)
        if (pago.nomBancoOrdExt) pagoNode.att('NomBancoOrdExt', pago.nomBancoOrdExt)
        if (pago.ctaOrdenante) pagoNode.att('CtaOrdenante', pago.ctaOrdenante)
        if (pago.rfcEmisorCuentaBen) pagoNode.att('RfcEmisorCtaBen', pago.rfcEmisorCuentaBen)
        if (pago.ctaBeneficiario) pagoNode.att('CtaBeneficiario', pago.ctaBeneficiario)
        if (pago.numOperacion) pagoNode.att('NumOperacion', pago.numOperacion)



        if (pago.documentos) {
          pago.documentos.forEach(function (cfdi) {
            let cfdiNode = pagoNode.ele('pago10:DoctoRelacionado')

            if (cfdi.idDocumento) cfdiNode.att('IdDocumento', cfdi.idDocumento)
            cfdiNode.att('MonedaDR', cfdi.monedaDR || 'MXN')
            
            if (cfdi.tipoCambioDR && cfdi.monedaDR != pago.monedaP)
              cfdiNode.att('TipoCambioDR', cfdi.tipoCambioDR)

						cfdiNode.att('MetodoDePagoDR', cfdi.metodoDePagoDR || 'PPD' )
            if (cfdi.numParcialidad) cfdiNode.att('NumParcialidad', cfdi.numParcialidad)
            if (cfdi.impSaldoAnt) cfdiNode.att('ImpSaldoAnt', cfdi.impSaldoAnt)
            if (cfdi.impPagado) cfdiNode.att('ImpPagado', cfdi.impPagado)
						cfdiNode.att('ImpSaldoInsoluto', cfdi.impSaldoInsoluto || 0)
          })
        }
      })
    }
  }


  return xml //.end()

}
