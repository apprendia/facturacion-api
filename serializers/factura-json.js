'use strict';

let serialize = function () {
	let _ = require('underscore');
	let Promise = require('promise');
	const moment = require('moment-timezone');

	return Promise.all([
		this.accountRef.child('fiscalEmisores').child(this.factura.emisor).once('value'),
		this.accountRef.child('fiscalReceptores').child(this.factura.receptor).once('value')
	]).then((results) => {
		let emisor = results[0].val(),
			receptor = results[1].val();

		return _.extend(
			_.omit(this.factura, 'emisor', 'receptor', 'certificado', 'conceptos', 'fechaUnix'), {
				receptor: (receptor) ? receptor.rfc : '',
				emisor: (emisor) ? emisor.rfc : '',
				fecha: moment(this.factura.fecha).utc().format(),
				id: this.facturaRef.key,
				pdfUrl: `https://api.miscfdis.com/facturas/${this.facturaRef.key}.pdf`,
				xmlUrl: `https://api.miscfdis.com/facturas/${this.facturaRef.key}.xml`,
			}
		);
	});
};

function FacturaJson(facturaSnap) {
	this.facturaRef = facturaSnap.ref;
	this.accountRef = this.facturaRef.parent.parent;
	this.factura = facturaSnap.val();

	this.serialize = serialize;
}


module.exports = FacturaJson;
