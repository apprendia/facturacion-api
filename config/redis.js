module.exports = function () {
  const { promisify } = require("util")

  const redis = require("redis")
  const client = redis.createClient()

  client.on('connect', () => console.log('Redis client connected'))
  client.on("error", (error) => console.error(error))

  const promisifyCommands = (obj, cmd) => {
    obj[cmd] = promisify(client[cmd]).bind(client)
    return obj
  }

  return [
    "get",
    "set",
    "expire",
    "ttl"
  ].reduce(promisifyCommands, {})
}();

