module.exports = function() {
	const { Client } = require('@elastic/elasticsearch')
	
	const client = new Client({ 
		node: 'http://elastic:elastic@localhost:9200',
		auth: {
			username: 'elastic',
			password: 'elastic'
		}
  })
  
	return client
}();