const Podio = require('podio-js').api
const clientId = 'facturacionmc'
const clientSecret = 'AdWDguthogb92Q3wyMlqy6c2lJl0ZI1nTBfSMrPZP7r3N0gwPLRJjoY3RzjP8Qyq'
const db = require('./admin').db
const Promise = require('promise')

const registeredApps = {}

function defaultClientOptions() {
  return {
    authType: 'app',
    clientId: clientId,
    clientSecret: clientSecret
  }
}


const PodioApps = {
  'default':{},

  'recargas': {
    id: '20989983',
    token: 'dad90d78112043d3b6762fb8fa6c82f3'
  },

  'recargas-new': {
    id: '22958661',
    token: '1542917481954ce7bc09e19fe0b44441'
  },

  'clientes': {
    id: '20990330',
    token: '9957f4a5c8f24679a3e096c49a3bb53b'
  },

  'clientes-new': {
    id: '23003083',
    token: '5f069cf5c76e4567b8ff94fcb2d55ef4'
  },

  'recargas-saeko': {
    id: '23003732',
    token: '1bf90c1731d942cd99cf7e4e605c7a27'
  }
}



module.exports = function() {
  return {
    client: getClient,
    registerApp: registerApp,
    reauthApp: registerApp,
    apps: PodioApps,
    registerAllApps: registerAllApps
  }
}()



function registerAllApps(){
  console.log('Registering all apps...')

  const promises = []

  for (var key in PodioApps) {
    if (key !== 'default' && PodioApps.hasOwnProperty(key) && PodioApps[key]) {
      promises.push(
        registerApp(key).then((c)=> registeredApps[key] = c)
      )
    }
 }

 return Promise.all(promises)
}




function registerApp(appName/*, appId, appToken, callback*/) {
  return new Promise(function(resolve, reject){
    try{
      const app = PodioApps[`${appName}`]
      
      if(!app) return reject(new Error('No app found'))
      
      const client = getClient(appName)
      client.isAuthenticated()
        .then(()=> resolve(client) )
        .catch(()=>{
          return client.authenticateWithApp(app.id, app.token, function(err) {
            if (err) return reject(new Error(err))
            client.isAuthenticated()
              .then(()=> resolve(client))
              .catch(reject)
          })
        })
    } catch(e){
      console.error('Error while registering Podio app: ', appName, ' ', e)
      return reject(e)
    }
  })
  // try {
  //   // console.log('Podio about to register app')
  //   let app = apps[`${appName}`] || {}
  //   app['appId'] = appId
  //   app['appToken'] = appToken

  //   let podio = getClient(appName)
  //   return podio.authenticateWithApp(appId, appToken, function(err) {
  //     // console.log('Podio did authenticateWithApp')

  //     if (err) throw new Error(err);
  //     podio.isAuthenticated().then(function() {
  //       // console.log('Podio app registered: ', appName)
  //       if (callback) {
  //         callback();
  //       }
  //     }).catch(function(e) {
  //       console.error('Error while registering Podio app: ', appName, ' ', e)
  //     });
  //   });
  // } catch (e) {
  //   console.error('Error while registering Podio app: ', appName, ' ', e)
  // }
}





function getClient(appName) {
  // console.log('getClient ', appName)

  appName = appName || 'default'

  if(!PodioApps[appName]) {
    return null
  }

  // if(registeredApps[appName]) {
  //   console.log('exists!')
  //   return registeredApps[appName]
  // }

  // console.log('doesnt exists!')
  return registeredApps[appName] = new Podio(
    defaultClientOptions(), 
    { sessionStore: new SessionStore(appName) }
  )
}





function SessionStore(appName) {
  appName = appName || 'default'
  let appRef = db.ref('podioTokens').child(appName)

  this.get = function(authType, callback) {
    return appRef.once('value').then(function(snap) {
      if (typeof callback === 'function') {
        let podioOAuth = (snap.val()) ? JSON.parse(snap.val()) : {}
        callback(podioOAuth)
      }
    })
  }

  this.set = function(podioOAuth, authType, callback) {
    return appRef.set(JSON.stringify(podioOAuth)).then(function(snap) {
      if (typeof callback === 'function') {
        callback(null)
      }
    })
  }
}
