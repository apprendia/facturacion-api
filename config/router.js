// Express
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const compression = require('compression');
const nodalytics = require('nodalytics');

module.exports = function() {
  let app = express();
  
//    app.use(cors()); // only for development
//    app.use(compression()); // only for development
    app.use(bodyParser.json()); // support json encoded bodies
    app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
    
    if (process.env.env == 'production'){
      app.use(nodalytics(''))
    }


    // POST /auth
    let loginController = require('../controllers/login');
    app.post('/auth', loginController.auth);



    // POST /dummy/webhook
    app.post('/dummy/webhook', function(req, res) {
        return res.send({
            sentData: req.body
        });
    });



    // GET /dummy
    app.get('/dummy', function(req, res) {
        return res.send('Hello!');
    });

    let exportablesController = require(global.rootPath('controllers/exportables'))
    app.get('/facturas/:id.pdf', exportablesController.pdf)
    app.get('/facturas/:id.xml', exportablesController.xml)
    
    let timbrarController = require(global.rootPath('controllers/timbrar'))
    app.post('/facturas/:accountId/:id/timbrar', timbrarController._timbrar)

    app.post('/facturas/sign', timbrarController.sign)
    app.post('/facturas/validate', timbrarController.validate)
    app.post('/facturas/timbrar', timbrarController.timbrar)
    
    
    
        
		// Podio webhooks listener
		// POST /podio
		app.all('/podio/:app', require(global.rootPath('controllers/podio')).default)
		app.all('/podio/recargas', require(global.rootPath('controllers/podio/recargas')).default)
		app.all('/podio/clientes', require(global.rootPath('controllers/podio/clientes')).default)
    
    
    // Admin actions
    let adminController = require('../controllers/admin/index');
    app.post('/admin/retrieveFacturasByDate', adminController.retrieveFacturasByDate);
    app.post('/admin/cancelFactura', adminController.cancelFactura);
    app.post('/admin/bulkCancel', adminController.bulkCancel);
    
    
    
		// Protected routes
		app.use(function(req, res, next) {
			console.log('> req.headers.authorization ', req.headers.authorization)
			if (!req.headers.authorization || req.headers.authorization.split(' ')[0] !== 'Bearer') {
				return res.status(401).json({ error: 'No credentials sent' })
			}
			
			next()
		});
    
    const usersController = require(global.rootPath('controllers/users'))
		// POST /users/:id/setup
    app.post('/users/:id/setup', usersController.setup)

    // POST /users/:id/token
    app.post('/users/:id/token', usersController.token)
    
		// POST /webhooks
    const webhooksController = require(global.rootPath('controllers/webhooks'))
    app.post('/webhooks', webhooksController.send)



    const PORT = process.env.env == 'production' ? 4000 : 4001;

    // Server start
    app.listen(PORT, function (){
	    global.APIPORT = PORT;
    	console.log('ContaMC server started ' + PORT);
    });
    
    return app;
}
