const session = require('express-session')
const MemoryStore = require('memorystore')(session)
const store = new MemoryStore({
  checkPeriod: 86400000 // prune expired entries every 24h
})

module.exports = {
  store: store,
  secret: 'FacturacionMC'
}
