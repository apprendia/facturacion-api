const _ = require('lodash')

module.exports = function PodioItem(itemdata) {
  // Set id
  _.set(this, 'id', _.get(itemdata, 'item_id'))

  // Set value of each field as attribute
  const fields = _.get(itemdata, 'fields')
  _.each(fields, (field) => {
    let values = _.get(field, 'values'),
      external_id = _.get(field, 'external_id')

    if (_.get(values, 'length') == 1) {
      values = _.get(values, '0')

      if (!!_.get(values, 'value')) {
        values = _.get(values, 'value')
      }
    }

    // console.log('external_id:', external_id, ' values:', values)

    _.set(this, external_id, values)
  })
}
