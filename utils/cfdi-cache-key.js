module.exports = function (xmlString) {
  const libxslt = require('libxslt')
  const xmlDoc = libxslt.libxmljs.parseXmlString(xmlString, { noblanks: true })

  const namespaces = {}
  xmlDoc.namespaces().forEach((el) => namespaces[el.prefix()] = el.href())

  const rfcE = xmlDoc.get('//cfdi:Comprobante/cfdi:Emisor/@Rfc', namespaces).value()
  const rfcR = xmlDoc.get('//cfdi:Comprobante/cfdi:Receptor/@Rfc', namespaces).value()

  const total = (() => {
    const Decimal = require('decimal.js-light')
    const rawTotal = xmlDoc.get('//cfdi:Comprobante/@Total', namespaces).value()
    return new Decimal(rawTotal).dividedToIntegerBy(1).toString()
  })()

  const fecha = (() => {
    const moment = require('moment-timezone')
    const rawFecha = xmlDoc.get('//cfdi:Comprobante/@Fecha', namespaces).value()
    return moment(rawFecha).startOf('minute').format('X')
  })()

  return [rfcE, rfcR, total].join(':')
}
