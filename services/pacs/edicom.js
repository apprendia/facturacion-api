'use strict';

module.exports = function() {
  return {
    timbrar: require('./edicom/timbrar'),
    cancelar: require('./edicom/cancelar'),
    retrieve: require('./edicom/retrieve'), // DEPRECATED
    retrieveByUUID: require('./edicom/retrieve'),
    retrieveByDate: require('./edicom/retrieve/byDate')
  }
}()
