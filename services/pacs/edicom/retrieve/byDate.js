process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

module.exports = function () {
  return function ({ rfc, from, to, format, filename, objectValueParser }) {
    let libxml = require('libxslt').libxmljs
    let forge = require('node-forge')
    let moment = require('moment-timezone')
    let _ = require('underscore')

    // Constants
    let { usr, pwd } = require('../constants')


    let args = _.omit({
      user: usr,
      password: pwd,
      rfc,
      iniDate: moment(from).format(),
      endDate: moment(to).format()
    }, _.isEmpty)



    console.log('retrieveByDate args: ', args)

    let connection = require('../connection')
    return connection.connect().then(function (client) {

      const { iniDate, endDate } = args;
      if (iniDate && endDate && moment(iniDate).isSame(endDate)) {
        return Promise.reject(new Error('Query dates cant be the same'))
      }

       return client['exportCfdiAsync'](args)

      //return { exportCfdiReturn: "UEsDBBQACAgIAHWAIU8AAAAAAAAAAAAAAABPAFgAUkZDX0NVQzA0MDExNVY0MV9VVUlEXzk3ODFEMDc4LTA3QTUtNDY4Qi04OTE5LTQ3OENGOEMyQzI3NV9GRUNfMTkwOTAxMDMyNzM3LnhtbHVwVAABV6DqrVJGQ19DVUMwNDAxMTVWNDFfVVVJRF85NzgxRDA3OC0wN0E1LTQ2OEItODkxOS00NzhDRjhDMkMyNzVfRkVDXzE5MDkwMTAzMjczNy54bWztWFmP4kgSfh9p/wPiFVX5wMZ2a2pGvm3wfQIvK98Yn9gG2/y0fd0/tglV3TPd2z2akXbeFqkwGRH5xZnhjPr516kqF7ek6/Omflsir/BykdRRE+d19rZ0HeGFXP76yz9++vnXKkqDaLh2wSLN47fli1In3eZqnaSXkOtVaS1M8XLRJenb8jQMbf8JgqKmHoIqevnYFwENr2neJWHQJ3nzGjUVFERRc62HHnrZ3ePSP8E+agk+6mGzN/rQx0bA/a6y5GHHF2XX6LXIm+76xP1QDUVBWYZBVPTQceuTKHer19q5mTRKc3EBA579HKVx/oltqrZrwqAekgWIR91/epDfwQH2OI6vfTC8Zk34Wk0Q4EHr5Yfg1H8tN65fmy6DUBhGoL2q2NEpqYKXvO6HoI6Sz7vyqi0bYNyPVHzmf5Gvkx+K1l9Q2yBrEPhHggbg9kC0zz/1T6sUoGF4pv0P/Fx8n9fnYOc/cxCwrk6Gd9HH9229fp36ePHHbv150M87vvz4I/Q6+QvAdfL4Q5A/AHxG7M9DPsXfvxH4AbtceJ8P1voVVIxyzYKOn9okzqMnFUPhDThwTt42XPK7InxbyoDaDI8CWa9h+BUGUmpTJ3HwtlT32nJhX8Nv+UISnQAbhRHqBSZf1oiDop/QzSccXy7YpBvyNI+CuAEAsizmHMsyEpvRo8zQmeyqHD2qHD++P2lY5dy1ykWoxhUjZx62u+Yon26RRpu8wpj0mGXirDrZqGUHzjNNjmMpRinUqyxaZcgyznG/RQNfa8OZKY42wxxFpA2r8h5L29MBrbkexTJP2raH2oMPe/B0+JvKwiKNuDybjZWLenNclefAJ7Oj6Gambw2Bj7fR2poPAHcktKssWHPgb5FYFMCTn3SORplM8xi6VxVYKEK/vAZ7DY4q4RxE+j3Mn7Y4Rz9GoqosDr75xA5FPrN9vArX2+EztmqRo/SV3wUjnBhgO1VEMHM/7M3rEaVypUIw1T6MW/oZhy3HIEycY5ktPvCnOpzptTaPmYlSvZIzUux7c1R5c3jndZUen/7y5iio3OEO4j6xd3r77sPBoQvkoFrFKIxPbJljGP7dnxIGcRWPvlUC33rVMkf+PQ8SRw9c7AtIIDncxUduhzvvqYz61KNYqurCgqs70ajd6ckpsLvqwaM/fuUny/G4fdxrY4ji94O/7Y9O8/3481oZ1o/1+Izjbzmj7l/nBqxFoQfyt4f8g5743jUW3VE6RZr6sIej7+pZhVWHx/0H7cx/TTuzLG1Hk3lnmPf4qI5eaIJTWrYzM55TlL7lbR3bsxi3KA2gywC8rVN4uscLqsyfthZ4WjCZmbCguYLnyALDmPDJM2E+c2H1N2y2+Puw6eZ/jr116PO73bAj8Zpn3h/nl5807wCrNn2T+ZZxS3kNcg7O7Ml0z9kkneno3R7wjv9vGdnmY9tx5VFzeEOlP87lpIqHtXcOxPgGeggjn2mNyYrLqchFaoQZ0BsEmtZZ2iTpB5/NduA3TxdJNCuGELXF6b7q7iVNrQ9eaSosvjJDGK0Q5bQlWkWGg3EtITcVn0bgYEee9P0hhyV3A8M797Y5czXl3igV3Z/2ylHbx6FE7vTQAMUV3VZondwvIild0o3ckUo2V3QqtE10ly8E7BpEP0NXUTjOztaunXDFM93JqBkDDpN8crTWSmhfHRPtcpHDQjYLue879e5pSECVN7+2bUk+r7shzmAoCbY9wV2JHFN2oSsXpLaeNNbtsFmNuC3Eavcj3lArCFMcercqfR6ypNuWdiOaBI1t3R2K6KppKVrL1RD1+6NZ7w9FRzfVkUqj+9z7Q7BlbzVLO1Vn2lsqsJWE9iZUzoWoYbbs2RX71W6UOdqkmWaCR/FOq49cSpbK0CnJ02eaVun+kbOYG0ECRlkcuW9z1b/nKuNpNRZxvui6YJQxzy585HDScMg8TRiE7a+T2tsbmaqaKLW0kZe3k3+Io9bbFfdarBPmyFUwbcxMfqfyibcH6rBTTc7mLvzxfm2onarBKnlS4F2S1U2jsJsymix1mvB0VYkJ4eMJVPUUizT4ivKuZyfp0YM0FdLG0HSsvncIWVo1d1+XSXxQbMT1W49BQIdK5sMq17BZSV10czbZhD5yikGo19LcQAbtoGkgwWVmsVI4timb7/wtAackRUWMTs5pIUw8me11E222ihHgjeCb4zBsVnh8NEs9Tcso2GmHim03V/S86XR9GK5zcFrvN2nc87gxWJ2HUwWnHOye2pBn3qr1wOx3dugZrSt73Q262Gd975wtIeuPIzHgkYle/FVZMphaJkrQjITHSjjEhNVZwG8ac4GuaimXPqasdt4MItyBYl9H3JRe4Q1yLfOYRylqr99RxkL6c34XT+fDIZLw+2BxCbUtmUySKh511qS+sXqR3XGun4qxC+fmhBV6Wxwq3DeOU6mVluGQZkZv5qNuiCttmFLBPBCKVBYxK6NMDElGeW6dvrT4pKfG6Yao3FHahykfXEZy1raKIPU81EdutDYsAYN2CdPaZUX1ew69iWtH048JMyieum1Mg4vmmpsvl5sVnwlMmPW9ChsyEZVMKU7YTBvOyM12fNzhJuk6uM1MW8yLcmyGcmsYpKPpoDZoFubbcqE1X11mYPBB4OcHg3ECJjYYBa5HSVkCptJsBppUKheiqYlbQfhtX7pwx3i0sY2l2sAs6IgjyJRsL1Q0XZIQ6WmuMYW9hZw2Fk/73pF2++bgrZBES7HYuWycKE4xTrlh+wYfzgejUuaQLHVrluNk7abbKTmwOo6mLhaeQtm8DtmRDnVkCidP5Lot08Bc00wCvVPQdl/zkm1tZ/y6YTSfmMaNizS2fO7So9wlesZGt6bk4TLyrSsVn+/kSLohLN3EAd+pgXDfi7J0aVe86aqrcrychM3eQzXccFdO0Zn8Oqh61k2F7fnSzivhLJ9qGSHNjdMJXEIQTrDzNieFTkUl7FluEJUWVYncHMNOP9L6tV6hq2qzSZG5P50GmUDHgml2x2B8e3sEuMvBNfUFXDubMgehJigUxR6rrgoeV2BwESXBnTUZmrh5Xxsuv/yYt/gq75tuYaXR25J1WZA5BME9DFkurCTLq6QW8v45HW1g5JHxKuyAMpbXHEtfuJrs8ZYtO7Ql6wtW1xxZAxxaWUigJyoioNGq4doLg2Yll6UXNrtcQB+arSRK2uGz7j2934PqQZ4VtFyAVLMCJ4PbN0z+ptb497/CMo8aMBcvxAQMoo+xDPoyOdZPwP6b9YItg1tidE0MInV7W5IbBEWIhxIWXPDzOACjK3DtKeXW72seA1q5pI+6vH2fD9igy5qFEXRRHpQPoQXY4wVl04E9Q9A9Ak9+XP/lqm26x+jwmQD9LSZZPOtaNr1QaYe3ZHrB8YAi6QvdcGhH9uiF7H2hMbTGPpN0WDCPTY6s/Jf5KP6t/V8owAHoB0EGw2ACymRofvl5SONPTv5I1XvRcHkGsMuPkRhwfzTefmfTX5mOv6fzT4+K39n8PdrtfTz93RyJvIIUua7MvS0pgkQ4mCBfYILGX7ANybyQFJj9MIJkBZJFWZTAP6bCJ/SzXX4zHRKf1sRHtwSV//+G+fc1zK9eW+AofPPmwjCCwNboRy6efCe8WIw3aF2bygjStzULVcxQ+O0Q6Mf+7G2KeLyq4cjjKMerMu5lhZnkVmOeeu4IUamDk/aFwe30dlGGNvCiG5jDUAxDzczS77M6Iz1pHHOGMGr+/qiQ06F38l3VreT8RPZXAh1kxqhISDD3a4rMrH5Yy3WfJqAAhBXJzha5adm933nVPR8ie78KFXYHuzv9OFZ7F56lOFZPK+I2+GlindBqq1+j1kVbQ4GQO3vVbYHLfcSqecedCxPfX0qOTTvxhJJcdERJR0r3J3GaeNZCMYIVPILwe2rWdhiCaI3sn4tUu7ilOCDitS3TNhm9qZXWzk3IPK/WjMNw1iVqOG2E0rKEywpGJL3LHrkAvR+0wdt7QkCTU3mQgw2Mmhb1Vdf5XZeBvv0/4S//+Ok/UEsHCF+Qh1PFCwAAPBUAAFBLAwQUAAgICAB1gCFPAAAAAAAAAAAAAAAATwBYAFJGQ19DVUMwNDAxMTVWNDFfVVVJRF9BQ0U5M0YxNi04M0M1LTQ0RjEtOERCRC1ENDk3MThFNDFBMDVfRkVDXzE5MDkwMTAzMDEzOS54bWx1cFQAAVKlpwVSRkNfQ1VDMDQwMTE1VjQxX1VVSURfQUNFOTNGMTYtODNDNS00NEYxLThEQkQtRDQ5NzE4RTQxQTA1X0ZFQ18xOTA5MDEwMzAxMzkueG1s7Vhbs6LIsn7fEfs/GL4aa3FX6Jg1E8VNQEDuiC8nuInIdQEK8utP6VrdM93TMzE74uy3Y4RiVWZ9WfllVZrpL79NVbm4pV2fN/XbEntFl4u0jpskr7O3peuIL/Tyt1///a9ffqviUxgP1y5cnPLkbfmi1mkpKOuubMbiQjFB12HLRZee3pbnYWj7LwgSN/UQVvHL57oYWng95V0ahX2aN69xUyFhHDfXeuiRl92clP4Z9XFL9HGPvHujj3wuhNKfGksf+/hm7Bq/FnnTXZ+4n6aROCzLKIyLHmFOBzwSL7Vp3sJNPDWCi+vQs1/iU5J/4Zqq7ZoorId0Afmo+y+P6Q9wiD2O42sfDq9ZE71WEwJlCLH8VJz67/VG4rXpMgRHUQw5aKodn9MqfMnrfgjrOP26Kq/asoGb+ysTX+Xf9Ov0L1Xrb6htmDUY+leKBpT23wDT5PqXiFAG9fr8S//cvQp3MjyPx9/wsfi5rM/hyv/JIbFdnQ4fqo/PG0G8Tn2y+Hv3/zno1xXfvvwdep3+B8B1+nhj2N8APpn955BP9Y9PDH3ALhfe1wtIvMKTpV6zsBOmNk3y+DlL4ugaXkwnbxs+/cNhfVvKcLYZHgcJQ2n0FYVaWlOnSfi21A76cmFfox/lYhqfoRhHMeYFpV8IzMHxLyj2BcOXCy7thvyUx2HSQABZ3uY8x7ESl4FRZkEmuxoPRo0Xxo8nQDXeJTQ+xnW+GHkzUHbNUT7fYh2YgsqaYMyy7V1zslHPAt4zTZ7nGFYttKu8tcqIY53jQcFDX2+jO1scbZY9brE2qso5kZRzgNd8j5OZJyltUHtocIBPR7hpHLoFmCtw2Vi5uHdPqvIS+nR23LqZ6VtD6FNtTFj3AOKOG/0qi9Y99BUs2YrwKUx7HuBspnss6DUVFYvIL6/hQUfjSryE8X6O8udenKOfYHFVFoFvPrGjrZDZPlVFhDJ8xdYsepS+87tgxTML984UMcrOwcG8HnEmVyuM1OxgVMCTB4VnMTbJyczePvCnOroDQr+PmYkzvZqzUuJ797jy7tEs7DUwPv0VzFHU+GCGvE/cDJQPHwIHFFigWcUojk9smWdZ4cOfEoW8bo++VULfes0yR+EjDhIPBj7xRSyUHP7dx27BLHgaqz3tqJamuajo7p141GcwOQU5ax46+uN3fnK8QNnHgz5GODUHvtIfnebn/At6GdWP8fjk8feYMfP3sYHjrdhD/dtD/zGf+t412bqjdI517bEfHszaRUM1R6D8x9xF+H7uwnHAjidzZtkPfjRnX+iiU1q2c2c9pyh9y1Mc27NYtygNaMuAMsUpvL0niJosnBULPi2UzkxU1F3Rc2SRZU307JmokLmo9js2V/z3sEHzf46tOODysW/UkQTdM+fH/RUm3QtQzQY3WWhZt5QJGHN4Z8+me8km6QLij/3AWuDPOrItJLbjyqPuCIYGPu/lpG0DwruE2+QGcwgrX4DOZsX7uci3zIiyMDeIAOw5YNLgIeeyHfwugCKN76ohxm1xnlfdXAKGCLzSVDlqZUYoXmHqWdm0qoyGIyFhN42aRuhgR5/3hyBHJXeNojv3tr7wNePeGA0/nA/qUT8kkUTv9pEBD1d8W+F1Or9vaen9tJY7Ws3uFTiJbRPP8vsGdY1Nf0euW/F4dxS7dqKVwHZno2YNNErzydFbKwW+Nqb6+7scFbJZyH3fabOnYyFT3vzatiX5QnRDkqFIGir9hr9uclLdRa5c0Dox6ZzbkXct5hWE0+cj1TArhFQdsFuVvoBY0k0BbgxomNiILijiq66f8Fquhrg/HM36EBQdaKojc4rne+8PocLdag44VWfaChPaagq8CZdzMW5Yhbu42361G2UemIBtJnTczkB7xFKyNBacaAFcANBA/4hZwo8wAKO8HfkfY9V/xCoTgJZsKaHounCUSc8ufCw46xRinicSIQ/XSevttcxUTXyy9FGQlckPkrj1dsVcb+uUPfIVCow7m89MPgn2wAQ7zeRt/l04zteG2Wk6qtFnFd2lWd00Krcu48nSpok6raptuvGpFKl6hsMaasV414uT9nggTYW0NvQ9Wc8dRpdWzc9EmSaBamOu33osBjNUeg9WuU7e1ZOLry8ml4Ijrxob7Vqaa8QADn4KJbTMLE6KxvbE5Ttf2aAnmmFidk/fT4U4CXR22Jt4o6hGSDWib47DsF5RydEs96dTGYc7Pai4dn3FL+tuvx+G6z08E4f1KekFyhiszqOYglcDu2fW9EWw6n1o9js78ozWlb3uhrzbl/3BuVhi1h/HzUDFJv7ur8qSJbUyVcNm3HicRCFsVF1E6qaz78hVK+XSJ9XVzrtDhjt42ImYn05XdI1dyzwRcIY57GectbD+ks/b8yUIYomaB4tPGaVkM0mqBNwh6P3a6rfcjnf90zZx0dycyGLfFkFF+cZxKvXSMhzazMD6ftwb25U+TCfRDDaqVBYJJ+NsgkhGeWmdvrSEtGfG6YZp/FE6RCchfB/pu66ootQLSB+7MWFYIonsUra1y4rpDzx+2xKOvj+m7KB6mtKYBh/fa/7+/n6zksuGFO/7g4Ya8iYu2XI7kXdgOCN/t5PjjjJp16FsdlJIL87JO5JbwyAdTQe3YbIw35YLvfmumEHhC0OfLxKlNuhmTTKwPErLEgr5fmBPhBDhnGY2h/6cp5lCE2qAmf1IrM5IJ9ljPoatqxSU3561PEPasb4xG2u641nkAMbTOjZJwfDOBdt5yq/2xUtSe97x62RfCu9s1NrCWtsXRZueGCztiyOmxNhO0axKzWc5b0/RxQ5R6BDWNK0/0JzDry53TDJuGi6tVrv63KyjmCj5GdmfhXtyacbZ4VMQlsI6W/eqKcasl6V12fEm3okgb7xbXs+Cc+jiou0321E4eRsQ3W+nU75DqXHTUIokGr4Lg3c7nNBGTlSVTaqCPobowWecGyrgCIWWu2sQNvpWmabIoE0TCyqkR8Fsl27tqwqlZIWs2CcZYV2jP4z0jkiaezd1k/n29iC4y2GZ+gLLzqbMIdUbBsexx6irwkcJDCNDwpo1HZqk+RgbrrD87MuEKu+bbmGd4rcl53IwchhGeSRcb6VZXqW1mPfPLmqNYo+IV1EHjXGC7lj7havLnmDZsgMseb/g9roj61AC1IUEc6K6hXNAM1x7YQBOcjmwsLnlAvm0bKVx2g5fbTuCbdHQBoE6OLVcuH3DibwMzw2G/m7W2nPQjqDKR8AKjrRwBFXdL2ygc5Jw/B2Za+ondP/DeMGV4S01uiaBnN3elvQaw7H1o2DnYKmfJyFsdqGTTy23/hgLJL1c8Gkfd3n70SlwYZc1CyPs4jwsH0oLWNV7Ydl0cM0Qdo8QfOsE5KptukcX8XXmD+1wmUJ6h+br7n795dEVfnn0sUJyffSDt7D/4X+D+kkEKK9VDa2IgqUDWJr9gRFV0CDdT16gI65lvC1VwRFREoP9jSZtNdUCD5z8Brv9TysQiQ3jc16WaRdClppFf12k71dov4QbhL1veB0sby9ANgzZBdILShE4gjM4wiIuEKQXeHBeMAqSiT2DgPy1i8h3Aflx3P+ZnV9/GU7JFyd/+P1xFPk8gzSXn102lP5V0/yTRf9Jz/0zm/+4Af3J4p/N3T6a3j90p9gr5NB1Zf5tCTiBIURs/UITHPVCkiL2QvMs/8KTzAajBRIDKPXZaz6hn0n4Tz0n8TUHw/v0/2n4v5eGv/sxtIHzw+8hSW42JIF/xuIpPyGcSe+cNBo9bxUKrkECPdZ0ah31DkcBe6OCzZXnxNJoa7S+7geF7pHAILDwmp61vhJCLCiQESMzNt1b6wBww4Tgu+IWEeq+P97uQTyzI4+Tq/W9M9XNlifcNUn7SJcf3XK8r/x3/DZ3usdQa9BGIIatNqK2qU45sOEUQ1A18ph09Zh3uWuu29veudIEQ6aCeVxvR/JIpqYaAEzagvRi8MPFlEC6m+cZU3f1sdUToy2UQ8Gn+NVVB/TA+NTGWSmIyfN0jXfcYZdtMdYXDrXeSiHuaTp9k+WAb68xUsWo4ffW+7W/8DeVo0NFmxqPqgjmenTm0ApDRVZUvJv8+5VQAGquToqxx5xN9ogF/EWBWf72ERCYtTQBxmCN4qbF/DxB/XHq84+fX//9r/8FUEsHCDGuXKEkDAAAuhUAAFBLAwQUAAgICAB1gCFPAAAAAAAAAAAAAAAATwBYAFJGQ19DVUMwNDAxMTVWNDFfVVVJRF8zNEM4MkQ1NS02RkFFLTQ0QjUtQTA0Mi0yMEFDOEJFQzY0QzZfRkVDXzE5MDkwMTAzMDUwOS54bWx1cFQAAa+QatlSRkNfQ1VDMDQwMTE1VjQxX1VVSURfMzRDODJENTUtNkZBRS00NEI1LUEwNDItMjBBQzhCRUM2NEM2X0ZFQ18xOTA5MDEwMzA1MDkueG1s7VhZj6PIsn4/0vkPll+tKnZjWlMzYl8MmB3DyxEGjDGbDdgsv/6mXVU90z3doznSnbdjqYwzIvKLLTOIqF9+G6tycU/bLm/qtyXyCi8XaR03SV5nb0vXEV42y99+/fe/fvmtio9R3N/aaHHMk7fli1qn5Q1Gi0ap7Ou9qIU0Wi7a9Pi2PPX9pfsCQXFT91EVv3zsi4GG12PepoeoS/PmNW4qKIrj5lb3HfSynZPSP8E+agk+6uGTN/jQx0bA/aGy9GHHV2W3+LXIm/b2xP1QDcVRWR6iuOgggeXol2pe39opoaz7aPgqCjz7JT4m+Re2qS5tc4jqPl2AeNTdlwf5HRxgD8Pw2kX9a9YcXqsRAjwIW34Ijt23cgP22rQZhMIwAu011Y5PaRW95HXXR3Wcfu7Kq0vZAON+puKT/1W+Tn8qWn9FvURZg8A/EzQAtwOiXf6le1qlAg39M+1/4efix7wuBzv/k4OAtXXav4s+vu8Y9jp2yeKv3fr7oJ87vv74K/Q6/S+A6/TxhyB/AfiM2N+HfIq/fyPwA3a58D4vFvYKTox6y6KWHy9pksdPKo7Ca3DhnPzScOkfDuHbUgbUpn8cEISA4VcYSGlNnSbR21Lb68uFfTt88jck9roBfCGNT4CNwgj1Am9eMMRB0S8w/gUmlws2bfv8mMdR0gAAWRZzjmUZic3oQWboTHY1jh40jh/enzSscS6mcTGqc8XAmYGybUL5dI912uRVxqSHLBMnzckGPQs4zzQ5jqUYtdBusmiVB5Zxwr2CRr5+OUxMEdoME4rI5VCVcyIppwCtuQ7FM09SLkHtwcEePB3+rrGwSCMuz2ZD5aLelFTlOfI3WSi6melbfeQTlxizpgDgDqR+kwVrinwFSUQBPPlxx9Eok+keQ3eaCgvFwS9v0V6H40o4R/FuPuRPW5zQT5C4KovAN5/YB5HPbJ+oDpjSf2Jr1maQvvG7YIQTA2ynihhm5mBv3kKUytUKwTU7GBT6GQeFYxAmyfHMFh/4Y32YaEyfhsxEqU7NGSnxvSmuvOkw8zuNHp7+8uYgaFwwg7iP7Ewr7z4EDl0ggWYVgzA8sWWOYfh3f0oYxFUMfasEvnWaZQ78ex4kju65xBeQSHK4q4/cg5n3NEZ76lEtTXNhwd058aDP9OgU+Kx58OAP3/jJcjxhh3t9OKDEHPhKFzrNj+PP6+WhfqyHZxx/zxk1f5sbsBaFDsjfH/IPeup7t0R0B+kU69rDHo6etbMGaw5P+A/amf+WdmZZ2o5Hc2aY9/hozq7QBae0bGdiPKcofctTHNuzGLcoDaDLADzFKbydxwuazJ8UCzwteJOZsKC7gufIAsOY8MkzYT5zYe13bLb457Dp5v8dW3Ho87vdsCPxumfOj/vLj7oXwJpN32X+wriljIGcgzt7Mt1zNkpnOn63B7zj/ywj23xiO6486A5vaPTHvRw1McC8cyQmd1BDGPlM60xWXE9FLlIDzIDaIND0jqXNDf3gs9kW/ObpIo0n1RDiS3GaV+1c0hQWeKWpssTKPMBohagnhbyoMhwNmITcNWIcgIPt5rTbBzksuWsY3rr39ZmrKfdOaej+tFdDfZ8cpM12dzDA4YrvK7RO56u4ka7Htdxu1Gyq6KNwaeJZvpKwa5DdBN1EIZwcxa6dw4pn2pNRMwZ8SPPR0S9WSvvakOrXq3woZLOQu67VZk9HIqq8+7VtS/IZa/skg6E0UjqSu5E5rm4PrlxsdGzUWbfFJy3mFIjV55BoqBWEqw69XZU+D1nSXaHdmN6Awoa1QRHfdP2I1nLVx90+NOt9ULR0U4XUMZ6nzu8jhb3XLO1UrWkrVGSrKe2NqJwLccMo7NkVu9V2kDnapJlmhAdxprVHLiVLY+jjhqfPNK3R3SNnCTeABAyyOHDf56p7z1XG01oiEnzRttEg455d+Ehw0gnIPI04hO9vo9bZa5mqmvho6QMvK6MfJPHF2xZzLdYpE3IVTBsTk89UPvJ2TwVbzeRs7sqH862htpoOa5uTCm/TrG4alV2X8Whp40gcV5WYkj6RQlVHsUhDrCjvdnbSDg2ksZDWhr7D67lFNqVVczNWpkmg2ojrXzwGARUqnYJVruOTenTR9dlkUzrkVIPUbqW5hgzaQY+RBJeZxUqH4XJk862vkPBxQ1Exs9tMx0IY+U2235loo6hGRDSCbw59v14RSWiWu+OxjKOtHlTsZX1Dz+t2t+v72xSdsP36mHQ8YfRW6xFUwamB3VHrzZm36l1kdlv74BkXV/baO3S1z7u9c7aErAsHsidiE736q7JkcK1M1agZSI+VCIg5VGeBuOvMFbpppVz6uLraehOIcAsOOxZz4/EGr5FbmSc8SlH73YwyFtKd81k8nYMgloi5t7iUUkomk6SKRx1ss1tbnchuOdc/iokL5+aIF7tLEVSEb4RjqZeW4WzMjF5P4c4QV3o/HgUzIFWpLBJWRpkEkozyfHG60uLTjhrGO6JxobQ/HPnoOmwmXVEFqeOhLnZjzLAEHNqmzMUuK6rbc+hdxBx9F6ZMr3qa0pgGF081N12vdys5k7gw7fYabMhkXDKlOOITbTgDN9lJuCXMjesQNjMquBfn+ATlVt9LoemgNigW5ttyoTffNDMw+CDw84PDBAmTa5wC7VFaloAZHNI1lJO6sUOh4HxURuI03c0Q3t6CKzUa6UFp/MFwLOE8Reb9SlZimWBbsNkcHYioOIsnrxrZiBCL0JgUu7J1oqCIPtIWtZ5Xvq/Sh1TDC71YrSZSIs4UMjumKVzTYOdw9qxezlMW6RR1d5lzJZ2njuR1ZXIR6hCwvc/a0EhZ66PYGcYk+6cEZ0lw2HmtMM7ZwKPecEwwd625Zj/jkgTsYzxPl4JajZwQN3aEGmI5R14cpi+8gt5GJQ2N8dY5JeH+cBYJC7q4SXeBhRMZEIcTEltzzJMHr5ajAb5O4xEh2X5d3K2BHxsa6nVtQGa4kVzkzq56hpH2wCiD5klPURusopT6tmrFYnh7ewS4zUGb+gLazqbMQahJCkWxx6qtokcLDBrRDehZ075Jmve14fLLBZd28S2te7DGHn3r8mMC46u8a9qFdYzflqzLglwiCOHhyHJhpVlepbWQd895aQ0jjzNQHVqgnuV1x9otXF32eMuWHdqSdwt2pzuyDji0upBAlVRFQKM1w7UXBs1KLksvbHa5gD40W2mcXvpP3SHNqxQBYzCmOcAdt2tYgZPflhwC/65WdWV7IfCWTuvcbhHSLNBL2wveZncMz+1+x2ab+gnefbdesGV0T422SUAc72/LzRpBEfLR5LOg/c+TCAy2wM2nlFu/r3l88x6+Nr+8Tw9s1GbNwojaOI/Kh9ACBRNHVDYt2NNH7SMtCEFuXh8zhlxdmvYxWXylfJ8K+M92/1N2In+yE6WIV/SPZn4SgEnQT2IJJsK0ejjw6y/9Mfni5I/svJ8TLs8AcvkxFwPuz2bcH2z6b0bkH+n82/PiDzb/iHZ/n1H/MEwiryCAritzIHM4u0E5gnhZCzT/guMM8ULDOPqCwjS7YXh2jbPrj9HwCf2smd+NiMQX+LNkgsP+v6r5z1XNb95dNu189/rCcZLEMfQjF08+eOGHGOu1h7HtV8omou4hnNXaimhHjGEN7kJtexXuWZm5lY146zb+4dJUPd/HlSUy7iazO0cM7wapoofTaoz0+VxhGLjNqoKBdzp/z1gMumLlWqVOE5oF5/uqRrfD7jIG3iBGZajWdr/PIbP27tx+4DVFz3cS4aj+CTvCR6tnxpan16TgHfd3QgnwwaN3aGIUWTu6tDsgnV0FocrTlgFBOQJfG26Fhj2bd4kZzIljR0a56f07Eo+2h7awOo2SeJw87YLd612wq+kWIs9KFAd9u4UaQ6ycNibXUVttbiFTX/ytgGEbyxKh6mRJ7R4ZlbI9beomnI5h0JGn63gi9zVuuzBFuLuBmUYfzh65AOUeFLb7e0JAoDUe5GANo6ZFfVN1/lBloO//Wfjrv//1f1BLBwjphoYRvAsAAEEVAABQSwMEFAAICAgAdYAhTwAAAAAAAAAAAAAAAE8AWABSRkNfQ1VDMDQwMTE1VjQxX1VVSURfODFFMkI1MzMtRkNBRC00RUQxLTgzREMtM0IxMEI5QTQwMzc0X0ZFQ18xOTA5MDEwMzAzMDQueG1sdXBUAAHTwjo4UkZDX0NVQzA0MDExNVY0MV9VVUlEXzgxRTJCNTMzLUZDQUQtNEVEMS04M0RDLTNCMTBCOUE0MDM3NF9GRUNfMTkwOTAxMDMwMzA0LnhtbO1YWa+jSpJ+b6n/g3VerXPYvEDpnttKNgMGzI7xy4jdmM1mMctPm9f5Y5P2OVV9q7rq6rY0/TaWbJwZkV9EfJEEGfz2j7EsFve4abO6en/B3tCXRVyFdZRV6fuLbfGv5Ms/fv/73377Rxkmftj1jb9Isuj95VWu4sLdBkL96tKmqdwDAi5t4uT95dx11/YLgoR11fll+Pq5LoQW3pKsiQO/jbP6LaxLxA/Duq+6Fnndz1HhnlEXN3gXd1aTM7jI50Io/amx+OHHN2N9+JZnddM/cT9NI6FfFIEf5i0y+L6z38X0yGymBvcFbitQMLLfwiTKvjB1eW3qwK+6eAH5qNovj+kPcIg9DMNb63dvaR28lSMCZQjx8qk4tt/rDcRb3aQIjqIYclRkMzzHpf+aVW3nV2H8dVVWXosaOvcrE1/l3/Sr+Jeq1TfUq5/WGPorRQ1K22+AcdT/EhHKoF6bfWmf3svQk+65Pf6Ej8XPZW0GV/5XBoltqrj7UH383gnibWyjxZ+H/9dBv6749ufP0Kv43wCu4scXw/4E8MnsX4d8qn/8YugD9mXhfL0BiTe4s+Q+9RtuvMZRFj5nVzi6gRveyq41G/9hs76/iHC27h4bCSMp9A2FWkpdxZH//qIc1ZeF2Qc/yvk4PEMxjmLUK0q+EpiF419Q/Au+flkwcdNlSRb6UQ0BRHGXsQxDC0wKBpEGqWgrLBgUlhs+rgBVWJtQ2BBX2XxgdU/a1yfxfA9VoHMyrYMhTXeTYqWDmnqso+ssy1C0nCu9uDOKgKGt01HCfVe9BhOdn0yaPu2wa1AWcyRIZw+v2BZfpY4gXb3KQb0jvFrcXWHQHcBsjkmH0sadKSqLi++S6Wlnp7prdL67voaEMXkQd9iqvcgbk+9KWLTj4ZUbDyzA6VR1aNAqMsrngVv0/lFFw5K/+OFhDrKnL9bJjbCwLHLP1Z/YwY5LTXddBoTUfcVWDHIQvos7p/kzDX2n8hClZ++o9yecyuQSWymmN0jgyYPE0hgdZavU3D3wxyqYAKFOQ6rjVCtntBC5zhSWzhTM3EEBwzNeTh94hfVmyPvIzED6iMGzQI55ipEP/PDEFlma5j7iKVDI6+7kGgWMrVUMfeA+8iCwoGMjl8d8wWJvLnb3Zs5RaOVpRzYUxUZ5+2CFgzqD0cpXs+Kggzt8FyfDcmvzdFSHAF/Pniu1J6v+Of+cWgTVYzw8efxnzqj5+9zA8Y5vof79of+Yj12nj3b2IJxDVXn4w4JZuSioYnFr9zF34b6fuzAMMMNRn2n6gx/FOuQqbxWGaU20Y+WFaziSZToGbeeFBm1pUCZZuXNwOF4RubNkwKuBkqmO8qrNO5bI07SOnh0d5VIbVf6JzeT/OWxQ/59jSxa4fPiNWgKnOvr8uH+5UXU8VDHBXeSutF2IBMw5vGfPun1JR+ECwg9/4FngX3VEk4tMyxYH1eI0BXzel6Oy8wjn4u+iO6whtHgBKp3mt3Oe7agBpWFt4AE4MEAnwUPOpHv4nwN5HE6yxofX/Dwvm7kAFOE5hS4z66UeoHiJyWdpe5VF1B8IAbsr63GAATbk+XD0MlSwNyi6t++bC1tR9p1S8OP5KJ/UYxQI5P4QaHBzhfclXsXzbUcKt2QjNqScTiVI+GsdzuJti9ratp2QfsefJksyKytYcnRz1ipaQ4M4Gy31asTAVYZYvd3EIBf1XGzbRpkdFfOp4u5WpimIF6LpohRFYl9qt2y/zVbyPrDFnFSJUWXsZjUpISshjDqf1jW1RFayBfbLwuUQQ7hLwA4BCQsb0Xh52Ktqgldi2YXt8aRXRy9vQF2eqCScp9btfIm5VwywykY3Jco35Rg4Iy5mfFjTEnOxd+1yP4gs0AFdj+iwm4HyyKVgKDRISA5cAFBA+8hZxA4wAYO4G9gfc9V+5CrlgBLt1lzeNP4grhwzdzHvrK4R/TyukNWxH5XW3IhUWYeJoQ6cKI2uF4VXZ5/P1a6K6RNbokCb6GymspEzO8rbKzprsjfuNPc1tVdUVCHPMrqP06quZWZThKOhjOM6WZa7eOuuY6RsKQar10vK6S9W3OKeMObCRlMPq2puMLIwKnYmijjyZBOz3atDY7BCxZO3zNTVJCc2vrnoTAxOrKxtlb7QN4gGLDzxBbRIDUYIhmvCZHtX2qIJSVEhfSCnJOdHjkyPBx2vJVnz1zXv6kPXbZbr6KQXhyQpQn+veiVz3fT4ZdMcDl3XT/6ZOG6SqOXWWmc0zprKWdkzW2pDXjijOvh6uzcDR7vaotPckZt5ORyti8Gn7WnYdutQx2/usijolVLEsl8PW4cR1ggdlBd+fVfpG9IrhVi4K3m5dybIcAM3OxGyY9KjG6wvsojDKep4mHHawNpLNu/OF88LhfXcGWxMSQWdCkLJ4RZBHjZGu2P2rO0mu8hGM31c5Ydr7pVrVzuNhVoYmkXqKdhMp4O2W6rdmPC6t5WFIo8YEacjRNCKy9VqC4OLW2oY75jCnoRjkHD+bSAnVZJ5oeWQNrRDQjP4FbKP6atZlFR7ZPH7jrDUwymmO9lRpFrX2HCq2Ol2uxvRZbvip8NRQTVxGxZ0sRtXE9CsgZ3M6LRf66RtrU16lFZOmK0mJDO6TjjpFm7CYqG/vyzU+rvDDAo/GPr8rND1Ft1uVhQ8HsVFAYXnuZv9pXUPrrEkVE6OJxKvG0NWa8imSLazcKhrKkiSYFcTPY0Zbt7XN8fERKxG8uuyb6lindxKsMVD5GYkEz0e7ItgBJ5GVhMWsajMdDrjW9uwzIDdHRM83LcpM0WDvgzC7F7zu5t0kvfDaEVn3O4YHen5VhbIwE/yoFW0um5VimZZ0iIdUxGRO34vjBQBvJdwJXU5kJtjRN38O34de8KqSiRO+ju/HOc1ddCOHC7ctQyRE7BvDrXorPqU1DqP5ROWPBcXvTk3gWRLwkbt2FtOnXdJKCv7tVpdTmNSjrx+PtmbJZUU5K0ekWNYO5Szv2Xycgu4O5SuDvGeXIsr9ioL+3Om3Q5dvh/A+/uD4CaDx9RXeOysiwxSvaVwHH+MmtJ/HIHhQZSEZ9a4q6P6Y6zZ3MtnX8aVWVs3CyMJ318Ym4GZw7C1s8JeFkacZmVc8Vn77KI2KPbIeBk00BjDqZZxWNiq6HCGKVrAEA8L5qBaogolQF4IsCbKOzgHFM02FxpgBJsBC5N5WSCflo04jK/dV9tHcDzC3YM9d9DLwm5rhmdF6OofzWr/899BkYU17J8Xu7iKm0f7hnzrMKsnYPvDeMEU/j3WmjqCTN3fX8gNhmObhxEGHvCzyIctLrTx1LKrjzG3goyxcRs22fWjP2D8Jq0Xmt+EmV88lBaQYscv6gau6fzmQfy3879YXuvm0Tt8nflDE1zEkNSu/urd7789esEvj+6Vi/pHF3j32x/eFlTP8EHRlxW0InEmZBTINGdYHwRzMBXKg3xOhnHYhgbdBYqEEhiOk4KwU1QTQG+r7B4XX41AINoPz1lRQBYhSfWi7RfxrYfmC+gfbHj9vjOcAwfJ0EQbCK/omsARnMIRGrEBJ7zC3fKKrSGX2DMHyK8jRL7Lx4/j9l/J+f23Lom+WNkj7I/9x2YpZLn4bK2h9Fed8k8W/TuN9s9s/uWu8yeLfzZ3/+h0/9CSYm+QQ9sWWbg7MQ6n1wTxyjOAfV1xLPZKEizzStAYSlNghRLb1WeD+YR+Vt4fGk3iC7r6LLzwJvr/2vufq73fPQFNYP3wEFytttsVgX/m4im/iKpWb1rckdu9SG/ongz41iaYenU7Crdh8hBy3Isk3+9lqjZg6E4nZPMwSARjjOTV01fLyqTnDJh81Q+Z26p6rJK4dJmizg218+FSxCJ9TRFTKYdKPphgYzsoJ5fJjWFNbd8ifpuc5yLtbd2rgzN+RxLOoVIxB+WoJkPO673pF2RhmVxG3GZEd8nreS/53lEB2VzqFJae+Kvc8zv07KfBleuON/WGgWXE0/eCpnLbqbl8pbb9LMhnePouWGLJilpsDnsJULCv304jMrZ9cXAEbdeDM23hDVGvp6neTEfWYVlmoDehdrwO64kKGS0lM3m+3XWaS/Dkanp3djoV8nrTSKeZ8V1A9fojF/AxAov8/SMhsGopHMzBBsV1g/p5gfrj1Ofbnt///rf/BVBLBwgJ6OWnJAwAAK8VAABQSwECFAAUAAgICAB1gCFPX5CHU8ULAAA8FQAATwBYAAAAAAAAAAAAAAAAAAAAUkZDX0NVQzA0MDExNVY0MV9VVUlEXzk3ODFEMDc4LTA3QTUtNDY4Qi04OTE5LTQ3OENGOEMyQzI3NV9GRUNfMTkwOTAxMDMyNzM3LnhtbHVwVAABV6DqrVJGQ19DVUMwNDAxMTVWNDFfVVVJRF85NzgxRDA3OC0wN0E1LTQ2OEItODkxOS00NzhDRjhDMkMyNzVfRkVDXzE5MDkwMTAzMjczNy54bWxQSwECFAAUAAgICAB1gCFPMa5coSQMAAC6FQAATwBYAAAAAAAAAAAAAACaDAAAUkZDX0NVQzA0MDExNVY0MV9VVUlEX0FDRTkzRjE2LTgzQzUtNDRGMS04REJELUQ0OTcxOEU0MUEwNV9GRUNfMTkwOTAxMDMwMTM5LnhtbHVwVAABUqWnBVJGQ19DVUMwNDAxMTVWNDFfVVVJRF9BQ0U5M0YxNi04M0M1LTQ0RjEtOERCRC1ENDk3MThFNDFBMDVfRkVDXzE5MDkwMTAzMDEzOS54bWxQSwECFAAUAAgICAB1gCFP6YaGEbwLAABBFQAATwBYAAAAAAAAAAAAAACTGQAAUkZDX0NVQzA0MDExNVY0MV9VVUlEXzM0QzgyRDU1LTZGQUUtNDRCNS1BMDQyLTIwQUM4QkVDNjRDNl9GRUNfMTkwOTAxMDMwNTA5LnhtbHVwVAABr5Bq2VJGQ19DVUMwNDAxMTVWNDFfVVVJRF8zNEM4MkQ1NS02RkFFLTQ0QjUtQTA0Mi0yMEFDOEJFQzY0QzZfRkVDXzE5MDkwMTAzMDUwOS54bWxQSwECFAAUAAgICAB1gCFPCejlpyQMAACvFQAATwBYAAAAAAAAAAAAAAAkJgAAUkZDX0NVQzA0MDExNVY0MV9VVUlEXzgxRTJCNTMzLUZDQUQtNEVEMS04M0RDLTNCMTBCOUE0MDM3NF9GRUNfMTkwOTAxMDMwMzA0LnhtbHVwVAAB08I6OFJGQ19DVUMwNDAxMTVWNDFfVVVJRF84MUUyQjUzMy1GQ0FELTRFRDEtODNEQy0zQjEwQjlBNDAzNzRfRkVDXzE5MDkwMTAzMDMwNC54bWxQSwUGAAAAAAQABABUAwAAHTMAAAAA" }
    }).then(function (result) {
      try {
        if (!result) throw 'No result'

        const response = result['exportCfdiReturn']
        let zip

        console.log('response', response);
        debugger;

        switch (format) {
          case 'path':
            let fs = require('fs')
            let path = require('path')

            zip = asZip(response)
            let savePath = filename || ''
            // Basic way to detect if provided `filename` is a dir
            // This isn't checking if dir (filename) exists
            if (_.isEmpty(savePath) || _.isEmpty(path.parse(savePath).ext)) {
              savePath = path.join(savePath, 'export.zip')
            }

            let data = zip.generate({ base64: false, compression: 'DEFLATE' })
            fs.writeFileSync(savePath, data, 'binary')

            return path.resolve(savePath)


          case 'zip':
            return asZip(response)



          case 'csv':
            objectValueParser = require(global.rootPath('models/factura/loadFromXml'))

          case 'array':
          case 'array-text':
          case 'object':
            zip = asZip(response)

            if (format !== 'array-text' && (_.isUndefined(objectValueParser) || _.isNull(objectValueParser))) {
              const parseString = require('xml2js').parseString
              const Promise = require('promise')

              objectValueParser = function (xmlString) {
                return new Promise(function (resolve, reject) {
                  const opts = {
                    // async: true
                  }

                  parseString(xmlString, opts, function (err, result) {
                    if (err) return reject(new Error(err))
                    resolve(result)
                  })
                })
              }
            }

            const reduce = require('async/reduce')
            const iniResult = format === 'object' ? {} : []
            const objPromise = reduce(zip.files, iniResult, (result, file, callback) => {
              let key = _keyNameFunction(file.name) || file.name

              if (_.isFunction(objectValueParser)) {
                const funResult = objectValueParser(file.asText())

                if (funResult.then) {
                  return funResult.then((i) => {
                    if (format === 'array' || format === 'array-text') {
                      result.push(i)
                    } else {
                      result[key] = i
                    }
                    return callback(null, result)
                  })
                }
              } else {
                if (format === 'array'|| format === 'array-text' ) {
                  result.push(file.asText())
                } else {
                  result[key] = file.asText()
                }
              }

              return callback(null, result)
            })

            if (format === 'csv') {
              // Build CSV
              return objPromise.then((obj) => {
                const ObjectsToCsv = require('objects-to-csv')
                const csv = new ObjectsToCsv(_.values(obj))
                return csv.toString()
              })
            } else {
              return objPromise
            }



          default: // raw
            return response
        }

        function asZip(response) {
          return new require('node-zip')(response, {
            base64: true,
            checkCRC32: true
          })
        }

        function extractZip(zip) {
          zip.files.forEach(() => { })
        }

        function _keyNameFunction(key) {
          return key
            .replace(/RFC/g, '')
            .replace(rfc, '')
            .replace(/UUID/g, '')
            .replace(/\_/g, '')
            .replace(/\./g, '')
            .replace(/xml/g, '')
        }

        // let comprobante = zip.files[`${factura.uuid}_.xml`]
        // let resultDoc = libxml.parseXmlString(comprobante.asText(), {
        // 	noblanks: true
        // })


        // let timbre = resultDoc.get('//tfd:TimbreFiscalDigital', {
        // 	tfd: 'http://www.sat.gob.mx/TimbreFiscalDigital'
        // })

        // return {
        // 	doc: resultDoc,
        // 	timbre: timbre
        // }
      } catch (e) {
        console.error(e);
        return Promise.reject(new Error(e))
      }
    }).catch(function (e) {
      console.error(e);
      return Promise.reject(new Error(e))
    })
  }
}()
