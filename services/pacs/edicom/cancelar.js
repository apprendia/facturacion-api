process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

module.exports = function () {
  return function (factura) {
    let libxml = require('libxslt').libxmljs
    let forge = require('node-forge')
    let moment = require('moment-timezone')
    let Promise = require('promise')

    // Constants
    let { usr, pwd } = require('./constants')

    try {
      let cert = forge.pki.certificateFromPem(factura.certificado.pemCer)
      let pki = forge.pki.privateKeyFromPem(factura.certificado.pemKey)
      let pkpass = 'fakepwd'
      var pfx = forge.pkcs12.toPkcs12Asn1(pki, cert, pkpass, { algorithm: '3des' })
      let pfx64 = forge.util.encode64(forge.asn1.toDer(pfx).getBytes())


      let args = {
        user: usr,
        password: pwd,
        rfcE: factura.emisor.rfc,
        rfcR: factura.receptor.rfc,
        total: factura.total,
        test: false,
        uuid: factura.uuid,
        pfx: pfx64,
        pfxPassword: pkpass
      }


      let connection = require('./connection')

      debugger

      return connection.connect().then(function (client) {
        console.log('cancelar', args);
        return client['cancelCFDiAsyncAsync'](args)
      }).then(function (result) {
        let response = result['cancelCFDiAsyncReturn']

        let ackXml = forge.util.decode64(response.ack)
        let resultDoc = libxml.parseXmlString(ackXml, { noblanks: true })

        ackXml = resultDoc.find('//s:CancelaCFDResult', { s: 'http://cancelacfd.sat.gob.mx' })
        let fechaCancelado = resultDoc.find('//@Fecha')[0].value()

        return {
          doc: ackXml,
          fechaCancelado: moment.tz(fechaCancelado, 'America/Mexico_City').utc().format()
        }
      })
    } catch (e) {
      return Promise.reject(`${e}`)
    }

  }
}()
