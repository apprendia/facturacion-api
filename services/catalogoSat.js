module.exports = function() {
	return {
		getName: getName
	}

	async function getName(catalogue, code) {
		let axios = require('axios')
		let Promise = require('promise')


		let host = 'https://elastic.miscfdis.com'
		let url = `${host}/${catalogue}/_doc/${code}/_source`

		return await axios.get(url).then(function(response) {
			return response.data.name
		}).catch(function(e) {
			console.log(e)
			return ''
		})
	}
}()

// https://elastic.miscfdis.com/sat3_3/c_ClaveUnidad/E48/_source
