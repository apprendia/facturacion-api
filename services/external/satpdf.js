'use strict';

module.exports = function () {
  const request = require('request')
  const options = {
    headers: {
      'Authorization': 'KmRsdJNPLjQfTxtHqVDp4W54NhN0QiMA7BepJFIPu2FbODVqpgWMZnlwdvc8sfxt',
      'Content-Type': 'application/json'
    }
  };

  function xmlTopdf(xml, callback) {
    request.post(
      'https://api.satpdf.com/v1/pdf',
      { ...options, json: { xml } },
      function (error, response, body) {
        if (error) console.error(error)

        const { pdf } = body
        const buffer = Buffer.from(pdf, 'base64')

        return callback(buffer)
      })
  }

  return {
    xmlTopdf
  }
}()
